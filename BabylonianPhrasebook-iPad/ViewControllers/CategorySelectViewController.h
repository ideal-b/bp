//
//  CategorySelectViewController.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 10.04.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Category.h"

@class CategorySelectViewController;

@protocol CategorySelectDelegate <NSObject>

@required
- (void)categorySelectVC:(CategorySelectViewController *)vc didSelectCategory:(Category *)category;

@end

@interface CategorySelectViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) id<CategorySelectDelegate> delegate;

@end
