//
//  CategorySelectViewController.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 10.04.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "CategorySelectViewController.h"
#import "CategoryCell.h"
#import "MainContainerViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+RGB.h"
#import "UIView+Sizes.h"
#import "PlayHavenSDK.h"
#import "MBProgressHUD.h"

@interface CategorySelectViewController ()

@property (nonatomic, weak) UILabel *buyFullDescrLabel;
@property (nonatomic, weak) UILabel *buyFullButtonLabel;
@property (nonatomic, weak) UILabel *moreAppsLabel;

@end

@implementation CategorySelectViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.fetchedResultsController = [Category MR_fetchAllSortedBy:@"sortOrder" ascending:YES withPredicate:nil groupBy:nil delegate:nil];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self populateBackground];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(languageChangedNotification:) name:BPLanguageChangedNotification object:nil];
    
#ifdef BP_LITE
    // if lite version
    NSArray *xibs = [[NSBundle mainBundle] loadNibNamed:@"AboutViewLite" owner:self options:nil];
    UIView *aboutView = xibs[0];
    
    UIButton *buyFullButton = (UIButton *)[aboutView viewWithTag:1];
    [buyFullButton addTarget:self action:@selector(buyFullButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *fb1Button = (UIButton *)[aboutView viewWithTag:2];
    [fb1Button addTarget:self action:@selector(fbButton1Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *fb2Button = (UIButton *)[aboutView viewWithTag:3];
    [fb2Button addTarget:self action:@selector(fbButton2Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *recommendedButton = (UIButton *)[aboutView viewWithTag:4];
    [recommendedButton addTarget:self action:@selector(recommendedButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.tableView.tableFooterView = aboutView;
    
    self.buyFullDescrLabel = (UILabel *)[aboutView viewWithTag:100];
    self.buyFullDescrLabel.text = BPLocalizedString(@"Interested");
    
    self.buyFullButtonLabel = (UILabel *)[aboutView viewWithTag:101];
    self.buyFullButtonLabel.text = BPLocalizedString(@"Buy full version");
    
    self.moreAppsLabel = (UILabel *)[aboutView viewWithTag:102];
#else    
    NSArray *xibs = [[NSBundle mainBundle] loadNibNamed:@"AboutView" owner:self options:nil];
    UIView *aboutView = xibs[0];
    
    UIButton *fb1Button = (UIButton *)[aboutView viewWithTag:1];
    [fb1Button addTarget:self action:@selector(fbButton1Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *fb2Button = (UIButton *)[aboutView viewWithTag:2];
    [fb2Button addTarget:self action:@selector(fbButton2Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *recommendedButton = (UIButton *)[aboutView viewWithTag:3];
    [recommendedButton addTarget:self action:@selector(recommendedButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.tableView.tableFooterView = aboutView;
    
    self.moreAppsLabel = (UILabel *)[aboutView viewWithTag:100];
#endif
    
    self.moreAppsLabel.text = BPLocalizedString(@"More apps");
}

- (void)populateBackground {
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.view.layer.frame;
    gradientLayer.colors = [NSArray arrayWithObjects:
                            (id)[UIColor whiteColor].CGColor,
                            (id)[UIColor colorWithR:198 G:196 B:192].CGColor,
                            nil];
    gradientLayer.locations = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0f],
                               [NSNumber numberWithFloat:1.0f],
                               nil];
    
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view.layer insertSublayer:gradientLayer atIndex:0];
}

- (void)buyFullButtonClicked {
#warning Set full app URL
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/app/id601748222"]];
    [Flurry logEvent:@"Social" withParameters:@{ @"Full Version": @"Opened" }];
}

- (void)fbButton1Clicked:(id)sender {
    NSURL *fbURL = [NSURL URLWithString:@"fb://profile/248206101856348"];
    if ([[UIApplication sharedApplication] canOpenURL:fbURL]) {
        [[UIApplication sharedApplication] openURL:fbURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.facebook.com/pages/Stupid-Casual/248206101856348"]];
    }
    
    [Flurry logEvent:@"Social" withParameters:@{ @"Visit site": @"Stupid Casual Facebook" }];
}

- (void)fbButton2Clicked:(id)sender {
    NSURL *fbURL = [NSURL URLWithString:@"fb://profile/199763443391107"];
    if ([[UIApplication sharedApplication] canOpenURL:fbURL]) {
        [[UIApplication sharedApplication] openURL:fbURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.facebook.com/pages/Ideal-Bureaucracy/199763443391107"]];
    }
    
    [Flurry logEvent:@"Social" withParameters:@{ @"Visit site": @"Ideal Bureaucracy Facebook" }];
}

- (void)recommendedButtonClicked:(id)sender {
    PHPublisherContentRequest *request = [PHPublisherContentRequest requestForApp:@"e47a438a3339400783ddddca490fd474" secret:@"24c46cd11be745ab9ee4733de9ea7af8" placement:@"moregames" delegate:self];
    [request send];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
    hud.dimBackground = YES;
    
    [Flurry logEvent:@"Social" withParameters:@{ @"More Games": @"Opened" }];
}


- (void)languageChangedNotification:(NSNotification *)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
        self.buyFullDescrLabel.text = BPLocalizedString(@"Interested");
        self.buyFullButtonLabel.text = BPLocalizedString(@"Buy full version");
        self.moreAppsLabel.text = BPLocalizedString(@"More apps");
        [self.tableView reloadData];
        [self.tableView selectRowAtIndexPath:selectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BPLanguageChangedNotification object:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.fetchedResultsController fetchedObjects] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
    
    Category *category = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell populateWithCategory:category forIndexPath:indexPath];
    
    return cell;    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [CategoryCell heightForIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Category *category = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self.delegate categorySelectVC:self didSelectCategory:category];
}

#pragma mark - PlayHaven delegate
- (void)request:(PHPublisherContentRequest *)request contentDidDisplay:(PHContent *)content {
    [MBProgressHUD hideHUDForView:self.view.window animated:YES];
}

- (void)request:(PHPublisherContentRequest *)request didFailWithError:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view.window animated:YES];
}

@end
