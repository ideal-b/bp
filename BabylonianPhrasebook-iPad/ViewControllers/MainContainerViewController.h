//
//  MainContainerViewController.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 10.04.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>

#define BPLanguageChangedNotification @"BPLanguageChangedNotification"

@interface MainContainerViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *leftContainerView;

@property (weak, nonatomic) IBOutlet UIView *centerContainerView;

@property (weak, nonatomic) IBOutlet UIImageView *currentLanguageImageView;

- (IBAction)currentLanguageButtonClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *radomButton;

@property (weak, nonatomic) IBOutlet UIButton *favoritesButton;

@end
