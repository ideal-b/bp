//
//  MainContainerViewController.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 10.04.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "MainContainerViewController.h"
#import "PhrasesViewController.h"
#import "RandomViewController.h"
#import "FavoritePhrasesViewController.h"
#import "CategorySelectViewController.h"
#import "Phrase.h"
#import <MessageUI/MessageUI.h>
#import "UIAlertView+Blocks.h"

@interface MainContainerViewController () <CategorySelectDelegate, MFMailComposeViewControllerDelegate, PhrasesViewControllerDelegate>

@property (nonatomic, strong) PhrasesViewController *phrasesVC;
@property (nonatomic, strong) RandomViewController *randomVC;
@property (nonatomic, strong) FavoritePhrasesViewController *favoritesVC;
@property (nonatomic, weak) CategorySelectViewController *categorySelectVC;

@property (nonatomic, weak) UIViewController *currentCenterVC;

@end

@implementation MainContainerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.radomButton setImage:[UIImage imageNamed:@"tabbar.button.random.hl.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
    [self.favoritesButton setImage:[UIImage imageNamed:@"tabbar.button.favorites.hl.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
    
    [self populateLangFlagImageOnButton];
    
    CategorySelectViewController *categorySelectVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CategorySelectVC"];
    categorySelectVC.delegate = self;
    [self addChildViewController:categorySelectVC];
    [self.leftContainerView addSubview:categorySelectVC.view];
    [categorySelectVC didMoveToParentViewController:self];
    self.categorySelectVC = categorySelectVC;
    
    PhrasesViewController *phrasesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PhrasesVC"];
    self.phrasesVC = phrasesVC;
    
    RandomViewController *randomVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RandomVC"];
    randomVC.delegate = self;
    self.randomVC = randomVC;
    
    FavoritePhrasesViewController *favoritesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FavoritesVC"];
    favoritesVC.delegate = self;
    self.favoritesVC = favoritesVC;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [categorySelectVC.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    [categorySelectVC tableView:categorySelectVC.tableView didSelectRowAtIndexPath:indexPath];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setCurrentCenterVC:(UIViewController *)currentCenterVC {
    if (_currentCenterVC) {
        [_currentCenterVC removeFromParentViewController];
        [_currentCenterVC.view removeFromSuperview];
    }
        
    if (currentCenterVC) {
        [self addChildViewController:currentCenterVC];
        [self.centerContainerView addSubview:currentCenterVC.view];
        [currentCenterVC didMoveToParentViewController:self];
    }
        
    _currentCenterVC = currentCenterVC;
}

- (void)viewDidUnload {
    [self setLeftContainerView:nil];
    [self setCurrentLanguageImageView:nil];
    [self setCenterContainerView:nil];
    [self setRadomButton:nil];
    [self setFavoritesButton:nil];
    [super viewDidUnload];
}

- (IBAction)addPhraseClicked:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setToRecipients:@[ kAddPhraseEmail ]];
        [mailViewController setSubject:BPLocalizedString(@"Add phrase letter subject")];
        [mailViewController setMessageBody:BPLocalizedString(@"Add phrase letter body") isHTML:NO];
        [self presentModalViewController:mailViewController animated:YES];
        
        [Flurry logEvent:@"Usage" withParameters:@{ @"Add Phrase": @"Opened" }];
    } else {
        [UIAlertView showAlertViewWithTitle:BPLocalizedString(@"Error")
                                    message:BPLocalizedString(@"No Mail Accounts")
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil onDismiss:nil onCancel:nil];
    }
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (IBAction)currentLanguageButtonClicked:(id)sender {
    [[AppLanguage sharedInstance] changeCurrentLanguageToNext];
    [self languageChanged];
    [self populateLangFlagImageOnButton];
}

- (void)languageChanged {
    [[NSNotificationCenter defaultCenter] postNotificationName:BPLanguageChangedNotification object:nil];
}

- (void)populateLangFlagImageOnButton {
    [self.currentLanguageImageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"flag.small_%@.png", BPCurrentLanguage()]]];
}

- (IBAction)randomButtonClicked:(id)sender {
    [self.categorySelectVC.tableView deselectRowAtIndexPath:[self.categorySelectVC.tableView indexPathForSelectedRow] animated:YES];
    self.radomButton.selected = YES;
    self.favoritesButton.selected = NO;
    [self setCurrentCenterVC:self.randomVC];
}

- (IBAction)favoritesButtonClicked:(id)sender {
    [self.categorySelectVC.tableView deselectRowAtIndexPath:[self.categorySelectVC.tableView indexPathForSelectedRow] animated:YES];
    self.radomButton.selected = NO;
    self.favoritesButton.selected = YES;
    [self setCurrentCenterVC:self.favoritesVC];
}

#pragma mark - CategorySelectDelegate
- (void)categorySelectVC:(CategorySelectViewController *)vc didSelectCategory:(Category *)category {
    self.radomButton.selected = NO;
    self.favoritesButton.selected = NO;
    
    self.phrasesVC.fetchedResultsController = [Phrase MR_fetchAllSortedBy:@"sortOrder" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"category == %@", category] groupBy:nil delegate:nil];
    [self.phrasesVC.swipeView setCurrentItemIndex:0];
    [self setCurrentCenterVC:self.phrasesVC];
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - PhrasesViewControllerDelegate methods
- (void)phrasesVC:(PhrasesViewController *)phrasesVC didChangePhrase:(Phrase *)phrase {
    NSIndexPath *indexPath = [self.categorySelectVC.fetchedResultsController indexPathForObject:phrase.category];
    [self.categorySelectVC.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];        
}

@end
