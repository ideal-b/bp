//
//  UIViewController+BackButton.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 30.01.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "UIViewController+NavbarButtons.h"
#import "UIView+Sizes.h"

@implementation UIViewController (NavbarButtons)

- (void)populateBackButton {
    UIImage *image = [UIImage imageNamed:@"backbutton.png"];
    UIImage *imageHighlighted = [UIImage imageNamed:@"backbutton.hl.png"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.size = image.size;
    [button setImage:image forState:UIControlStateNormal];
    [button setImage:imageHighlighted forState:UIControlStateHighlighted];
    [button addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)populateLangButton {
    UIImage *buttonBackgroundImage = [UIImage imageNamed:@"button.flag.png"];
    UIImage *buttonBackgroundImageHl = [UIImage imageNamed:@"button.flag.hl.png"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.size = buttonBackgroundImage.size;
    [button setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
    [button setBackgroundImage:buttonBackgroundImageHl forState:UIControlStateHighlighted];
    [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"flag.small_%@.png", BPCurrentLanguage()]] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(changeLanguage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *langItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = langItem;
}

- (void)changeLanguage:(UIButton *)button {
    [[AppLanguage sharedInstance] changeCurrentLanguageToNext];
    [self languageChanged];
    [self populateLangFlagImageOnButton];
}

- (void)languageChanged {
    [[NSNotificationCenter defaultCenter] postNotificationName:BPLanguageChangedNotification object:nil];
}

- (void)populateLangFlagImageOnButton {
    UIButton *button = (UIButton *)self.navigationItem.leftBarButtonItem.customView;
    [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"flag.small_%@.png", BPCurrentLanguage()]] forState:UIControlStateNormal];
}

@end
