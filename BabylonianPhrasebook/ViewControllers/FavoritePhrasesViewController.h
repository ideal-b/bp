//
//  FavoritePhrasesViewController.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 01.03.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhrasesViewController.h"

@interface FavoritePhrasesViewController : PhrasesViewController

@end
