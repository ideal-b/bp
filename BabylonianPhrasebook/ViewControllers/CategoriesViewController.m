//
//  CategoriesViewController.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 28.03.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "CategoriesViewController.h"
#import "UIViewController+NavbarButtons.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+RGB.h"
#import "Category.h"
#import "CategoryCell.h"
#import "PhrasesViewController.h"
#import <MessageUI/MessageUI.h>
#import "UIAlertView+Blocks.h"
#import "UILabel+Size.h"
#import "PlayHavenSDK.h"
#import "MBProgressHUD.h"

@interface CategoriesViewController () <MFMailComposeViewControllerDelegate> {
    BOOL _sidebarVisible;
}

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) Category *selectedCategory;
@property (nonatomic, weak) UILabel *titleLabel;
@property (nonatomic, weak) UILabel *buyFullDescrLabel;
@property (nonatomic, weak) UILabel *buyFullButtonLabel;

@end

@implementation CategoriesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, -4, 320, 44)];
    titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithName:@"DR Agu" size:18];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    [titleView addSubview:titleLabel];
    
    self.navigationItem.titleView = titleView;
    self.titleLabel = titleLabel;
    
//    self.versionLabel.text = [NSString stringWithFormat:@"Version: %@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"GitVersion"]];
    self.versionLabel.text = @"";
    
    self.fetchedResultsController = [Category MR_fetchAllSortedBy:@"sortOrder" ascending:YES withPredicate:nil groupBy:nil delegate:nil];
    self.view.clipsToBounds = YES;
	
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
        
    [self populateLangButton];
    [self populateBackground];
    
    self.titleLabel.text = BPLocalizedString(@"Title");
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(languageChangedNotification:) name:BPLanguageChangedNotification object:nil];
    
#ifdef BP_LITE
    // if lite version
    NSArray *xibs = [[NSBundle mainBundle] loadNibNamed:@"BuyFullVersionView" owner:self options:nil];
    UIView *buyFullVersionView = xibs[0];
    UIButton *buyFullButton = (UIButton *)[buyFullVersionView viewWithTag:1];
    
    UILabel *textLabel = (UILabel *)[buyFullVersionView viewWithTag:2];
    self.buyFullDescrLabel = textLabel;
    self.buyFullDescrLabel.text = BPLocalizedString(@"Interested");
    
    UILabel *buyLabel = (UILabel *)[buyFullVersionView viewWithTag:3];
    self.buyFullButtonLabel = buyLabel;
    self.buyFullButtonLabel.text = BPLocalizedString(@"Buy full version");
    
    [buyFullButton addTarget:self action:@selector(buyFullButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView = buyFullVersionView;
#endif
    
    self.moreAppsLabel.text = BPLocalizedString(@"More apps");
    
}

- (void)buyFullButtonClicked {
#warning Set full app URL
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/app/id601748222"]];
    [Flurry logEvent:@"Social" withParameters:@{ @"Full Version": @"Opened" }];
}

- (void)languageChangedNotification:(NSNotification *)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.titleLabel.text = BPLocalizedString(@"Title");
        self.buyFullDescrLabel.text = BPLocalizedString(@"Interested");
        self.buyFullButtonLabel.text = BPLocalizedString(@"Buy full version");
        self.moreAppsLabel.text = BPLocalizedString(@"More apps");
        [self.tableView reloadData];
    });
}

- (void)populateBackground {
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.view.layer.frame;
    gradientLayer.colors = [NSArray arrayWithObjects:
                            (id)[UIColor whiteColor].CGColor,
                            (id)[UIColor colorWithR:198 G:196 B:192].CGColor,
                            nil];
    gradientLayer.locations = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0f],
                               [NSNumber numberWithFloat:1.0f],
                               nil];
    
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view.layer insertSublayer:gradientLayer atIndex:0];
}

- (IBAction)plusButtonClicked:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setToRecipients:@[ kAddPhraseEmail ]];
        [mailViewController setSubject:BPLocalizedString(@"Add phrase letter subject")];
        [mailViewController setMessageBody:BPLocalizedString(@"Add phrase letter body") isHTML:NO];
        [self presentModalViewController:mailViewController animated:YES];
        
        [Flurry logEvent:@"Usage" withParameters:@{ @"Add Phrase": @"Opened" }];
    } else {
        [UIAlertView showAlertViewWithTitle:BPLocalizedString(@"Error")
                                    message:BPLocalizedString(@"No Mail Accounts")
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil onDismiss:nil onCancel:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated {

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setSidebar:nil];
    [self setVersionLabel:nil];
    [self setRecommendedLabel:nil];
    [self setMoreAppsLabel:nil];
    [super viewDidUnload];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BPLanguageChangedNotification object:nil];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {    
    return [[self.fetchedResultsController fetchedObjects] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
    
    Category *category = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell populateWithCategory:category forIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [CategoryCell heightForIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Category *category = [self.fetchedResultsController objectAtIndexPath:indexPath];
    self.selectedCategory = category;
    [self performSegueWithIdentifier:@"showPhrases" sender:self];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showPhrases"]) {
        PhrasesViewController *vc = segue.destinationViewController;
        vc.fetchedResultsController = [Phrase MR_fetchAllSortedBy:@"sortOrder" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"category == %@", self.selectedCategory] groupBy:nil delegate:nil];        
        vc.title = [self.selectedCategory name];
        
        vc.navigationItem.rightBarButtonItem = self.navigationItem.rightBarButtonItem;
    }
}

- (IBAction)showHideSidebarClicked:(id)sender {
    if (_sidebarVisible) {
        [self.sidebar hideSidebar];
        _sidebarVisible = NO;
    } else {
        [self.sidebar showSidebar];
        _sidebarVisible = YES;
    }
}

#pragma mark MFMailComposeViewControllerDelegate methods
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)fbButton1Clicked:(id)sender {
    NSURL *fbURL = [NSURL URLWithString:@"fb://profile/248206101856348"];
    if ([[UIApplication sharedApplication] canOpenURL:fbURL]) {
        [[UIApplication sharedApplication] openURL:fbURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.facebook.com/pages/Stupid-Casual/248206101856348"]];
    }
    
    [Flurry logEvent:@"Social" withParameters:@{ @"Visit site": @"Stupid Casual Facebook" }];
}

- (IBAction)fbButton2Clicked:(id)sender {
    NSURL *fbURL = [NSURL URLWithString:@"fb://profile/199763443391107"];
    if ([[UIApplication sharedApplication] canOpenURL:fbURL]) {
        [[UIApplication sharedApplication] openURL:fbURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.facebook.com/pages/Ideal-Bureaucracy/199763443391107"]];
    }
    
    [Flurry logEvent:@"Social" withParameters:@{ @"Visit site": @"Ideal Bureaucracy Facebook" }];
}

- (IBAction)recommendedButtonClicked:(id)sender {
    PHPublisherContentRequest *request = [PHPublisherContentRequest requestForApp:@"e47a438a3339400783ddddca490fd474" secret:@"24c46cd11be745ab9ee4733de9ea7af8" placement:@"moregames" delegate:self];
    [request send];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    
    [Flurry logEvent:@"Social" withParameters:@{ @"More Games": @"Opened" }];
}

#pragma mark - PlayHaven delegate
- (void)request:(PHPublisherContentRequest *)request contentDidDisplay:(PHContent *)content {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)request:(PHPublisherContentRequest *)request didFailWithError:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

@end
