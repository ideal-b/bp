//
//  PhrasesViewController.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 1/28/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "PhrasesViewController.h"
#import "Phrase.h"
#import "UIView+Sizes.h"
#import "UIViewController+NavbarButtons.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+RGB.h"
#import "Category.h"
#import "PhraseView.h"
#import "UIAlertView+Blocks.h"
#import <MessageUI/MessageUI.h>
#import "UIImage+Size.h"
#import "SHK.h"
#import "UILabel+Size.h"
#import "FavoritePhrasesViewController.h"
#import "SHKShareItemDelegate.h"
#import "SHKTwitter.h"

@interface PhrasesViewController () <MFMailComposeViewControllerDelegate, PhraseViewDelegate, SHKShareItemDelegate> {
    CGSize _lngButtonSize;
    CGFloat _lngButtonBottomMargin;
    CGFloat _delta;
}

@property (nonatomic, assign) BOOL backgroundInitialized;

@property (nonatomic, strong) NSArray *langButtons;

@property (nonatomic, strong) NSString *selectedLanguage;

@property (nonatomic, strong) NSArray *languages;

@property (nonatomic, weak) UIImageView *selectedLangView;

@property (nonatomic, strong) NSString *lastCurrentLanguage;

@end

@implementation PhrasesViewController 

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IS_IPAD()) {
        _delta = 60;
    } else {
        _delta = 40;
    }
    
    self.backgroundInitialized = NO;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        [self populateBackButton];
    }    
    
    self.swipeView.alignment = SwipeViewAlignmentCenter;
    self.swipeView.pagingEnabled = YES;
    self.swipeView.defersItemViewLoading = YES;
    self.swipeView.wrapEnabled = NO;
    self.swipeView.itemsPerPage = 1;
    self.swipeView.truncateFinalPage = YES;
    self.swipeView.dataSource = self;
    self.swipeView.delegate = self;
    
    self.lastCurrentLanguage = BPCurrentLanguage();
    
    if (IS_IPAD()) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(currentLangChanged:) name:BPLanguageChangedNotification object:nil];
    }    
}

- (void)currentLangChanged:(NSNotification *)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self initializeLangButtons];
        NSString *translateLang = [AppLanguage sharedInstance].currentTranslateLanguage;
        if (!translateLang) {
            [self changeSelectedLanguageToLanguageAtIndex:0];
        } else {
            [self changeSelectedLanguageToLanguageAtIndex:[self.languages indexOfObject:translateLang]];
        }        
    });
}

- (void)initializeLangButtons {
    NSString *currentLanguage = BPCurrentLanguage();
    Phrase *phrase = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    NSMutableArray *phraseArray = [NSMutableArray arrayWithArray:[phrase.langPhrases allObjects]];
    NSIndexSet *indexSet = [phraseArray indexesOfObjectsPassingTest:^BOOL(LangPhrase *phrase, NSUInteger idx, BOOL *stop) {
        BOOL result = [phrase.lang isEqualToString:currentLanguage];
        if (result) {
            stop = YES;
        }
        return result;
    }];
    
    [phraseArray removeObjectAtIndex:indexSet.firstIndex];
    
    NSString *systemPreferredLang = [AppLanguage sharedInstance].systemPreferredLanguage;
    LangPhrase *secondPhrase = nil;
    if (![systemPreferredLang isEqualToString:currentLanguage] && ([[AppLanguage sharedInstance].languages indexOfObject:systemPreferredLang] != NSNotFound)) {
        // if system preferred language is known for us
        NSIndexSet *is = [phraseArray indexesOfObjectsPassingTest:^BOOL(LangPhrase *phrase, NSUInteger idx, BOOL *stop) {
            BOOL result = [phrase.lang isEqualToString:systemPreferredLang];
            if (result) {
                stop = YES;
            }
            return result;
        }];
        
        secondPhrase = [phraseArray objectAtIndex:is.firstIndex];
        [phraseArray removeObjectAtIndex:is.firstIndex];
    }
    
    [phraseArray sortUsingDescriptors:@[ [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES] ]];
    if (secondPhrase) {
        [phraseArray insertObject:secondPhrase atIndex:0];
    }
    
    NSMutableArray *langs = [NSMutableArray arrayWithCapacity:phraseArray.count];
    for (LangPhrase *lp in phraseArray) {
        [langs addObject:lp.lang];
    }
    
    self.languages = [NSArray arrayWithArray:langs];    
    
    NSMutableArray *langButtons = [NSMutableArray new];
    
    BOOL isIpad = IS_IPAD();
    
    if (!isIpad) {
        _lngButtonSize = CGSizeMake(58, 50);
        _lngButtonBottomMargin = 7;
    } else {
        _lngButtonSize = CGSizeMake(81, 71);
        _lngButtonBottomMargin = 25;
    }
    
    CGFloat leftOffset = (self.view.width - _lngButtonSize.width * 5) * 0.5f;
    
    if (!self.selectedLangView) {
        UIImage *image = [UIImage imageNamed:@"language.active.png"];        
        
        UIImageView *selectedLangView = [[UIImageView alloc] initWithImage:image];
        [self.view addSubview:selectedLangView];
        
        CGFloat offsetY = 0;
        if (IS_IPAD()) {
            offsetY = 3;
        }
        
        selectedLangView.origin = CGPointMake(leftOffset + 5, self.view.height - _lngButtonSize.height - _lngButtonBottomMargin - offsetY);
        self.selectedLangView = selectedLangView;
    }
    
    for (NSInteger i = 0; i < 5; i++) {
        UIButton *button;
        if (self.langButtons && (self.langButtons.count > 0)) {
            button = [self.langButtons objectAtIndex:i];
        } else {
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(leftOffset + _lngButtonSize.width * i, self.view.height - _lngButtonSize.height - _lngButtonBottomMargin, _lngButtonSize.width, _lngButtonSize.height);
            button.tag = i;            
            [button addTarget:self action:@selector(langButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
            [langButtons addObject:button];
        }
        
        NSString *lang = [self.languages objectAtIndex:i];
        
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"flag.big_%@.png", lang]];        
        
        [button setImage:image forState:UIControlStateNormal];
    }
    
    if (langButtons.count > 0) {
        self.langButtons = [NSArray arrayWithArray:langButtons];
    }
}

- (void)changeSelectedLanguageToLanguageAtIndex:(NSInteger)index {
    self.selectedLanguage = [self.languages objectAtIndex:index];    
    [AppLanguage sharedInstance].currentTranslateLanguage = self.selectedLanguage;
    
    CGSize buttonSize = _lngButtonSize;
    
    CGFloat leftOffset = (self.view.width - buttonSize.width * 5) * 0.5f;
    
    [UIView animateWithDuration:0.2f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.selectedLangView.origin = CGPointMake(leftOffset + 5 + buttonSize.width * index, self.selectedLangView.origin.y);
    } completion:^(BOOL finished) {
    }];
    
    NSInteger currentItemIndex = self.swipeView.currentItemIndex;
    [self.swipeView reloadData];
    [self.swipeView setCurrentItemIndex:currentItemIndex];
}

- (void)langButtonClicked:(UIButton *)button {
    [self changeSelectedLanguageToLanguageAtIndex:button.tag];
}

- (void)refresh {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self refreshTitle];
        [self.swipeView reloadData];
        [self.swipeView setCurrentItemIndex:self.currentIndex];
    });
}

- (void)refreshTitle {
    Phrase *phrase = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:self.currentIndex inSection:0]];
    self.title = [phrase.category valueForKey:[NSString stringWithFormat:@"name_%@", BPCurrentLanguage()]];
}

- (void)dealloc {
    if (IS_IPAD()){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:BPLanguageChangedNotification object:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!self.backgroundInitialized && !IS_IPAD()) {
        CAGradientLayer *gradientLayer = [CAGradientLayer layer];
        gradientLayer.frame = self.swipeView.layer.bounds;
        gradientLayer.colors = [NSArray arrayWithObjects:
                                (id)[UIColor whiteColor].CGColor,
                                (id)[UIColor colorWithR:198 G:196 B:192].CGColor,
                                nil];
        gradientLayer.locations = [NSArray arrayWithObjects:
                                   [NSNumber numberWithFloat:0.0f],
                                   [NSNumber numberWithFloat:1.0f],
                                   nil];
        gradientLayer.cornerRadius = self.swipeView.layer.cornerRadius;
        [self.swipeView.layer insertSublayer:gradientLayer atIndex:0];
        self.backgroundInitialized = YES;
    }    
    
    if ((self.langButtons.count == 0 || ![self.lastCurrentLanguage isEqualToString:BPCurrentLanguage()]) && ([[self.fetchedResultsController fetchedObjects] count] > 0)) {
        self.lastCurrentLanguage = BPCurrentLanguage();
        [self initializeLangButtons];
    }    
    
    NSString *translateLang = [AppLanguage sharedInstance].currentTranslateLanguage;
    if (!translateLang) {
        [self changeSelectedLanguageToLanguageAtIndex:0];
    } else {
        [self changeSelectedLanguageToLanguageAtIndex:[self.languages indexOfObject:translateLang]];
    }
    
    NSInteger count = self.fetchedResultsController.fetchedObjects.count;
    
    if (self.currentIndex >= count) {
        self.currentIndex = 0;
    }
        
    [self.swipeView setCurrentItemIndex:self.currentIndex];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(phrasesVC:didChangePhrase:)] && count > 0) {
        Phrase *phrase = [self phraseForIndex:self.swipeView.currentItemIndex];
        [self.delegate phrasesVC:self didChangePhrase:phrase];
    }
    
    BOOL hidden;
    if (count > 0) {
        hidden = NO;
    } else {
        hidden = YES;
    }
    
    for (UIButton *b in self.langButtons) {
        b.hidden = hidden;
    }
    self.selectedLangView.hidden = hidden;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setSwipeView:nil];
    [self setFetchedResultsController:nil];
    [super viewDidUnload];
}

- (IBAction)plusButtonClicked:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setToRecipients:@[ kAddPhraseEmail ]];
        [mailViewController setSubject:BPLocalizedString(@"Add phrase letter subject")];
        [mailViewController setMessageBody:BPLocalizedString(@"Add phrase letter body") isHTML:NO];
        [self presentModalViewController:mailViewController animated:YES];
        
        [Flurry logEvent:@"Usage" withParameters:@{ @"Add Phrase": @"Opened" }];
    } else {
        [UIAlertView showAlertViewWithTitle:BPLocalizedString(@"Error")
                                    message:BPLocalizedString(@"No Mail Accounts")
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil onDismiss:nil onCancel:nil];
    }
}

#pragma mark - SwipeViewDataSource methods
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView {
    return ([[self.fetchedResultsController fetchedObjects] count] + 1);
}

- (UIView<SwipeItemView> *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index {    
    if (index == [[self.fetchedResultsController fetchedObjects] count]) {
        static NSString *itemLastIdentifier = @"LastPhraseIdentifier";
        
        UIView<SwipeItemView> *view = [swipeView dequeueItemViewWithIdentifier:itemLastIdentifier];
        if (!view) {
            NSString *nibname;
            if (IS_IPAD()) {
                nibname = @"PhraseLastViewIpad";
            } else {
                nibname = @"PhraseLastView";
            }
            
            NSArray *xibs = [[NSBundle mainBundle] loadNibNamed:nibname owner:self options:nil];
            PhraseLastView *phraseView = xibs[0];
            phraseView.height = swipeView.height;
            phraseView.width = swipeView.width - _delta;
            phraseView.reuseIdentifier = itemLastIdentifier;
            
            view = phraseView;
        }
        
        UILabel *label  = (UILabel *)[view viewWithTag:1];
        label.text = BPLocalizedString(@"Add phrase text");
        [label sizeToFitFixedWidth:label.width];
        
        return view;
    } else {
        static NSString *itemIdentifier = @"PhraseIdentifier";
        
        UIView<SwipeItemView> *view = [swipeView dequeueItemViewWithIdentifier:itemIdentifier];
        if (!view) {
            PhraseView *phraseView = [[PhraseView alloc] initWithFrame:CGRectMake(0, 0, swipeView.width - _delta, swipeView.height) bottomMargin:_lngButtonBottomMargin + _lngButtonSize.height];
            phraseView.reuseIdentifier = itemIdentifier;
            phraseView.delegate = self;
            view = phraseView;
        }
        
        PhraseView *phraseView = (PhraseView *)view;
        Phrase *phrase = [self phraseForIndex:index];
        
        [self populatePhraseView:phraseView withPhrase:phrase];
        
        return view;
    }    
}

- (Phrase *)phraseForIndex:(NSInteger)index {
    return [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
}

- (void)populatePhraseView:(PhraseView *)phraseView withPhrase:(Phrase *)phrase {
    NSString *currentLanguage = BPCurrentLanguage();
    NSString *selectedLanguage = self.selectedLanguage;
    
    assert(currentLanguage != nil);
    assert(selectedLanguage != nil);
    
    LangPhrase *mainLangPhrase = [LangPhrase MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"(phrase == %@) AND (lang == %@)", phrase, currentLanguage]];
    LangPhrase *secondLangPhrase = [LangPhrase MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"(phrase == %@) AND (lang == %@)", phrase, selectedLanguage]];            
    
    assert(mainLangPhrase != nil);
    assert(secondLangPhrase != nil);
    
    [phraseView populateWithMainLangPhrase:mainLangPhrase secondLangPhrase:secondLangPhrase isFavorite:[phrase.favorite boolValue]];
}

#pragma mark - SwipeViewDelegate methods
- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView {    
    self.currentIndex = swipeView.currentItemIndex;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(phrasesVC:didChangePhrase:)]) {
        Phrase *phrase = [self phraseForIndex:swipeView.currentItemIndex];
        [self.delegate phrasesVC:self didChangePhrase:phrase];
    }
    
    for (id view in swipeView.visibleItemViews) {
        if ([view isKindOfClass:[PhraseView class]]) {
            PhraseView *pView = view;
            if (pView != swipeView.currentItemView) {
                [pView scrollToTop];
            }
        }
    }
    
    if (![self isKindOfClass:[FavoritePhrasesViewController class]]) {
        [[iRate sharedInstance] logEvent:NO];
    }
}

#pragma mark MFMailComposeViewControllerDelegate methods
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - PhraseViewDelegate methods
- (void)phraseView:(PhraseView *)phraseView didClickedShareButton:(UIButton *)shareButton withPhrase:(Phrase *)phrase withMainLangPhrase:(LangPhrase *)langPhrase {
    SHKItem *item = [SHKItem text:langPhrase.text];    
    item.URL = [NSURL URLWithString:@"http://stupidcasual.ru/phrasebook/"];
    SHKActionSheet *actionSheet = [SHKActionSheet actionSheetForItem:item];
    actionSheet.shareDelegate = self;
    [SHK setRootViewController:self];
    
    if (IS_IPAD()) {
        CGRect rect = [shareButton convertRect:shareButton.bounds toView:self.view];
        [actionSheet showFromRect:rect inView:self.view animated:YES];
    } else {
        [actionSheet showInView:self.view.window];
    }
}

#pragma mark - SHKShareItemDelegate methods
-(BOOL)aboutToShareItem:(SHKItem*)item withSharer:(SHKSharer *)sharer {
//    NSLog(@"SHARER: %@", [[sharer class] sharerTitle]);
    if ([sharer isKindOfClass:[SHKTwitter class]]) {
        item.text = [NSString stringWithFormat:@"%@ %@", item.text, @"http://stupidcasual.ru/phrasebook/"];
    }

    return YES;
}

@end
