//
//  UIViewController+BackButton.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 30.01.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>

#define BPLanguageChangedNotification @"BPLanguageChangedNotification"

@interface UIViewController (NavbarButtons)

- (void)populateBackButton;
- (void)populateLangButton;
- (void)languageChanged;
- (void)populateLangFlagImageOnButton;

@end
