//
//  FavoritePhrasesViewController.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 01.03.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "FavoritePhrasesViewController.h"
#import "Category.h"

@interface FavoritePhrasesViewController ()

@end

@implementation FavoritePhrasesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    if (!IS_IPAD()) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.swipeView.currentItemIndex inSection:0];
        Phrase *phrase = [self.fetchedResultsController objectAtIndexPath:indexPath];
        self.title = [phrase.category valueForKey:[NSString stringWithFormat:@"name_%@", BPCurrentLanguage()]];
    }
    
    self.fetchedResultsController = [Phrase MR_fetchAllSortedBy:@"favorite" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"favorite > 0"] groupBy:nil delegate:nil];
    
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView {
    [super swipeViewCurrentItemIndexDidChange:swipeView];
    
    if (!IS_IPAD()) {
        if (swipeView.currentItemIndex >= 0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:swipeView.currentItemIndex inSection:0];
            Phrase *phrase = [self.fetchedResultsController objectAtIndexPath:indexPath];
            self.title = [phrase.category valueForKey:[NSString stringWithFormat:@"name_%@", BPCurrentLanguage()]];
        } else {            
            if (swipeView.numberOfItems == 0) {
                self.title = @"";
            }
        }
    }
    
}

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView {
    return [[self.fetchedResultsController fetchedObjects] count];
}

@end
