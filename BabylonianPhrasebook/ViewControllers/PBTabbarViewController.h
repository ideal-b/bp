//
//  PBTabbarViewController.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 31.01.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PBTabbarViewController : UIViewController

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@property (strong, nonatomic) UIViewController *currentViewController;
@property (weak, nonatomic) IBOutlet UIView *placeholderView;

@property (strong, nonatomic) NSArray *viewControllers;

@end
