//
//  SecondViewController.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 15.01.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhrasesViewController.h"

@interface RandomViewController : PhrasesViewController

- (void)shuffle;

@end
