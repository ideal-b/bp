//
//  CategoriesViewController.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 28.03.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SidebarView.h"

@interface CategoriesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet SidebarView *sidebar;
- (IBAction)showHideSidebarClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
- (IBAction)fbButton1Clicked:(id)sender;
- (IBAction)fbButton2Clicked:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *recommendedLabel;
- (IBAction)recommendedButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *moreAppsLabel;

@end
