//
//  FavoritesViewController.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 15.01.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "FavoritesViewController.h"
#import "LangPhrase.h"
#import "FavoriteCell.h"
#import "FavoritePhrasesViewController.h"
#import "Phrase.h"
#import "UIViewController+NavbarButtons.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+RGB.h"

@interface FavoritesViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) Phrase *selectedPhrase;

@end

@implementation FavoritesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self populateBackground];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.rowHeight = 60;
}

- (void)populateBackground {
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.view.layer.frame;
    gradientLayer.colors = [NSArray arrayWithObjects:
                            (id)[UIColor whiteColor].CGColor,
                            (id)[UIColor colorWithR:198 G:196 B:192].CGColor,
                            nil];
    gradientLayer.locations = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0f],
                               [NSNumber numberWithFloat:1.0f],
                               nil];

    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view.layer insertSublayer:gradientLayer atIndex:0];
}

- (void)viewWillAppear:(BOOL)animated {
    self.title = BPLocalizedString(@"Favorites");
    [self createFetchedResultsController];
    [self.tableView reloadData];
    [super viewWillAppear:animated];
}

- (void)refresh {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.title = BPLocalizedString(@"Favorites");
        [self createFetchedResultsController];
        [self populateLangFlagImageOnButton];
        [self.tableView reloadData];
    });
}

- (void)dealloc {

}

- (void)createFetchedResultsController {
    self.fetchedResultsController = [LangPhrase MR_fetchAllSortedBy:@"phrase.favorite" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"(phrase.favorite > 0) AND (lang == %@)", BPCurrentLanguage()] groupBy:nil delegate:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setFetchedResultsController:nil];
    [self setSelectedPhrase:nil];
    [super viewDidUnload];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showFavoritePhrases"]) {
        FavoritePhrasesViewController *vc = segue.destinationViewController;
        vc.fetchedResultsController = [Phrase MR_fetchAllSortedBy:@"favorite" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"favorite > 0"] groupBy:nil delegate:vc];
        NSInteger index = [vc.fetchedResultsController indexPathForObject:self.selectedPhrase].row;
        vc.currentIndex = index;
        vc.navigationItem.rightBarButtonItem = self.navigationItem.rightBarButtonItem;
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.fetchedResultsController fetchedObjects] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CellIdentifier";
    
    FavoriteCell *cell = (FavoriteCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[FavoriteCell alloc] initWithReuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    LangPhrase *phrase = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell populateWithLangPhrase:phrase forIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    LangPhrase *phrase = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return [FavoriteCell heightForLangPhrase:phrase];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    LangPhrase *lp = [self.fetchedResultsController objectAtIndexPath:indexPath];
    self.selectedPhrase = lp.phrase;
    [self performSegueWithIdentifier:@"showFavoritePhrases" sender:self];
}

@end
