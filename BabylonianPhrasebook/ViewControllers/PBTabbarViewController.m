//
//  PBTabbarViewController.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 31.01.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "PBTabbarViewController.h"
#import "RandomViewController.h"

@interface PBTabbarViewController ()

@end

@implementation PBTabbarViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UINavigationController *categoriesRootVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoriesRootVC"];
    UINavigationController *randomRootVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RandomRootVC"];
    UINavigationController *favoritesRootVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FavoritesRootVC"];
    
    self.viewControllers = @[ categoriesRootVC, randomRootVC, favoritesRootVC ];
    
    NSAssert(self.viewControllers.count == self.buttons.count, @"Number of view controllers must be equal to number of tabbar buttons");
    
    for (UIButton *button in self.buttons) {
        UIImage *hlImage = [button imageForState:UIControlStateHighlighted];
        [button setImage:hlImage forState:(UIControlStateHighlighted | UIControlStateSelected)];
        
        [button addTarget:self action:@selector(tabbarButtonClicked:) forControlEvents:UIControlEventTouchDown];
        
        if (button.selected) {
            [button sendActionsForControlEvents:UIControlEventTouchDown];
        }
    }
}

- (void)tabbarButtonClicked:(id)sender {
    for (UIButton *button in self.buttons) {
        [button setSelected:NO];
    }
    
    UIButton *button = sender;
    [button setSelected:YES];
    
    UIViewController *dst = [self.viewControllers objectAtIndex:button.tag];
    
    if (self.currentViewController == dst) {
        if ([dst isKindOfClass:[UINavigationController class]]) {
            UINavigationController *navDst = (UINavigationController *)dst;
            [navDst popToRootViewControllerAnimated:YES];
            
            if ([navDst.visibleViewController isKindOfClass:[RandomViewController class]]) {
                RandomViewController *rndVC = (RandomViewController *)navDst.visibleViewController;
                [rndVC shuffle];
            }
        }
    } else {
        for (UIView *view in self.placeholderView.subviews) {
            [view removeFromSuperview];
        }
        
        [self.currentViewController removeFromParentViewController];
        
        self.currentViewController = dst;
        [self addChildViewController:dst];
        [self.placeholderView addSubview:dst.view];
        [dst didMoveToParentViewController:self];
    }
        
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setButtons:nil];
    [self setCurrentViewController:nil];
    [self setPlaceholderView:nil];
    [super viewDidUnload];
}

@end
