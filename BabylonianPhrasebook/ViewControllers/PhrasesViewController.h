//
//  PhrasesViewController.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 1/28/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"
#import "Phrase.h"
#import "PhraseLastView.h"

@class PhrasesViewController;

@protocol PhrasesViewControllerDelegate <NSObject>

@optional
- (void)phrasesVC:(PhrasesViewController *)phrasesVC didChangePhrase:(Phrase *)phrase;

@end

@interface PhrasesViewController : UIViewController <NSFetchedResultsControllerDelegate, SwipeViewDataSource, SwipeViewDelegate>

@property (weak, nonatomic) IBOutlet SwipeView *swipeView;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, weak) id<PhrasesViewControllerDelegate> delegate;

- (void)populatePhraseView:(UIView *)phraseView withPhrase:(Phrase *)phrase;

- (Phrase *)phraseForIndex:(NSInteger)index;

- (void)refreshTitle;

- (IBAction)plusButtonClicked:(id)sender;

@end
