//
//  FavoritesViewController.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 15.01.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OverlapTableView.h"

@interface FavoritesViewController : UIViewController
@property (weak, nonatomic) IBOutlet OverlapTableView *tableView;

@end
