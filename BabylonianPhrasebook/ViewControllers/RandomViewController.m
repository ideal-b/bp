//
//  SecondViewController.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 15.01.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "RandomViewController.h"
#import "UIView+Sizes.h"
#import "Category.h"
#import "UIColor+RGB.h"
#import "PhraseView.h"
#import "UIViewController+NavbarButtons.h"

@interface RandomViewController ()

@property (nonatomic, strong) NSArray *randomIndexes;

@end

@implementation RandomViewController

- (void)viewDidLoad
{
    self.fetchedResultsController = [Phrase MR_fetchAllSortedBy:@"sortOrder" ascending:YES withPredicate:nil groupBy:nil delegate:self];
    
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = nil;
    
    NSInteger count = [[self.fetchedResultsController fetchedObjects] count];
    NSMutableArray *randomIndexes = [NSMutableArray arrayWithCapacity:count];
    for (NSInteger i = 0; i < count; i++) {
        [randomIndexes addObject:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    self.randomIndexes = [NSArray arrayWithArray:randomIndexes];
}

- (void)viewWillAppear:(BOOL)animated {
    [self shuffleIndexes];
    
    NSIndexPath *indexPath = [self.randomIndexes objectAtIndex:0];
    Phrase *phrase = [self.fetchedResultsController objectAtIndexPath:indexPath];    
    self.title = [phrase.category valueForKey:[NSString stringWithFormat:@"name_%@", BPCurrentLanguage()]];
    
    [super viewWillAppear:animated];
}

- (void)shuffle {
    [self shuffleIndexes];
    
    NSIndexPath *randomIndexPath = [self.randomIndexes objectAtIndex:self.swipeView.currentItemIndex];
    Phrase *phrase = [self.fetchedResultsController objectAtIndexPath:randomIndexPath];
    self.title = [phrase.category valueForKey:[NSString stringWithFormat:@"name_%@", BPCurrentLanguage()]];
    
    [self.swipeView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    [self setRandomIndexes:nil];
}

- (void)shuffleIndexes
{    
    static BOOL seeded = NO;
    if(!seeded)
    {
        seeded = YES;
        srandom(time(NULL));
    }
    
    NSMutableArray *newRandomIndexes = [NSMutableArray arrayWithArray:self.randomIndexes];
    
    NSUInteger count = [newRandomIndexes count];
    for (NSUInteger i = 0; i < count; ++i) {
        // Select a random element between i and end of array to swap with.
        int nElements = count - i;
        int n = (random() % nElements) + i;
        [newRandomIndexes exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    
    self.randomIndexes = [NSArray arrayWithArray:newRandomIndexes];
}

- (Phrase *)phraseForIndex:(NSInteger)index {
    NSIndexPath *randomIndexPath = [self.randomIndexes objectAtIndex:index];
    Phrase *phrase = [self.fetchedResultsController objectAtIndexPath:randomIndexPath];
    return phrase;
}

- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView {
    [super swipeViewCurrentItemIndexDidChange:swipeView];
    
    if (swipeView.currentItemIndex < self.randomIndexes.count) {
        NSIndexPath *randomIndexPath = [self.randomIndexes objectAtIndex:swipeView.currentItemIndex];
        Phrase *phrase = [self.fetchedResultsController objectAtIndexPath:randomIndexPath];
        self.title = [phrase.category valueForKey:[NSString stringWithFormat:@"name_%@", BPCurrentLanguage()]];
    }  else {
        self.title = @"";
    }
}

@end
