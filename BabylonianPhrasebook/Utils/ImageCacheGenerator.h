//
//  ImageCacheGenerator.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 2/19/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageCacheGenerator : NSObject

- (void)generateFavoriteCellsBackground;

- (void)generatePhraseBackground;

+ (ImageCacheGenerator *)sharedInstance;

@end
