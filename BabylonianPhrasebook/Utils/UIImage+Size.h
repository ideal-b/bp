//
//  UIImage+Size.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 4/12/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Size)

- (UIImage *)imageScaledToSize:(CGSize)newSize;

@end
