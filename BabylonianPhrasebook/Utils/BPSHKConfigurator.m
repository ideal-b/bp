//
//  BPSHKConfigurator.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 5/6/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "BPSHKConfigurator.h"

@implementation BPSHKConfigurator

- (NSString*)appName {
	return @"Phrasebook";
}

- (NSString*)appURL {
	return @"http://stupidcasual.ru/phrasebook/";
}

- (NSString*)vkontakteAppId {
	return @"3597720";
}

- (NSString*)facebookAppId {
	return @"449719938435612";
}

- (NSString*)facebookLocalAppId {
#ifdef BP_LITE
    return @"lite";
#else
    return @"full";
#endif
}

- (NSString*)twitterConsumerKey {
	return @"AHWBRjDaQDib4Kx79Lyw";
}

- (NSString*)twitterSecret {
	return @"v4GSsIv2EwQtTYJuK9TRza4cF6PgqLDoPmQrC760";
}

- (NSString*)twitterCallbackUrl {
	return @"http://stupidcasual.ru/phrasebook/";
}

- (NSNumber*)showActionSheetMoreButton {
	return [NSNumber numberWithBool:NO];// Setting this to true will show More... button in SHKActionSheet, setting to false will leave the button out.
}

- (NSArray*)defaultFavoriteTextSharers {
    return [NSArray arrayWithObjects:@"SHKVkontakte",@"SHKTwitter",@"SHKFacebook", nil];
}

- (NSNumber*)maxFavCount {
	return [NSNumber numberWithInt:3];
}

@end
