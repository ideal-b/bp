//
//  AppLanguage.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 2/21/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "AppLanguage.h"

static NSString *kSystemLanguageKey = @"bp-system-lang";
static NSString *kUserLanguageKey = @"bp-user-lang";

@interface AppLanguage()

@property (nonatomic, strong) NSBundle *currentBundle;

@end

@implementation AppLanguage

+ (AppLanguage *)sharedInstance
{
    static dispatch_once_t onceQueue;
    static AppLanguage *appLanguage = nil;
    
    dispatch_once(&onceQueue, ^{ appLanguage = [[self alloc] init]; });
    return appLanguage;
}

- (id)init {
    self = [super init];
    if (self) {
        self.languages = @[ @"ru", @"en", @"fr", @"de", @"es", @"et" ];
        
        NSString *language = [self preferredLanguage];
        
        if ([self.languages indexOfObject:language] != NSNotFound) {
            _currentLanguage = language;
        } else {
            _currentLanguage = @"en";
        }
        
        [self saveUserLanguage];
        
        self.currentBundle = [self bundleForCurrentLanguage];
        
        [Flurry logEvent:@"Usage" withParameters:@{ @"Language": self.systemPreferredLanguage }];
    }
    
    return self;
}

- (NSBundle *)bundleForCurrentLanguage {
    NSString *path = [[NSBundle mainBundle ] pathForResource:_currentLanguage ofType:@"lproj"];
    NSBundle *bundle = [NSBundle bundleWithPath:path];
    return bundle;
}

- (NSString *)systemPreferredLanguage {
    NSString * currentPreferredLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    currentPreferredLanguage = [currentPreferredLanguage lowercaseString];
    return currentPreferredLanguage;
}

- (NSString *)preferredLanguage {
    NSString * currentPreferredLanguage = self.systemPreferredLanguage;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *systemSavedLanguage = [defaults stringForKey:kSystemLanguageKey];
    if (!systemSavedLanguage) {
        [defaults setValue:currentPreferredLanguage forKey:kSystemLanguageKey];
        [defaults synchronize];
        return currentPreferredLanguage;
    } else {
        if ([systemSavedLanguage isEqualToString:currentPreferredLanguage]) {
            NSString *userSavedLanguage = [defaults stringForKey:kUserLanguageKey];
            if (userSavedLanguage) return userSavedLanguage;
            else return currentPreferredLanguage;
        } else {
            [defaults setValue:currentPreferredLanguage forKey:kSystemLanguageKey];
            [defaults synchronize];            
            return currentPreferredLanguage;
        }
    }
}

- (void)saveUserLanguage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:_currentLanguage forKey:kUserLanguageKey];
    [defaults synchronize];
}

- (void)changeCurrentLanguageToNext {
    self.currentTranslateLanguage = nil;
    
    NSInteger index = [self.languages indexOfObject:self.currentLanguage];
    NSInteger newIndex = (index + 1) % self.languages.count;
    _currentLanguage = [self.languages objectAtIndex:newIndex];
    
    [self saveUserLanguage];
    
    self.currentBundle = [self bundleForCurrentLanguage];
}

- (NSString *)localizedStringForTag:(NSString *)tag {
    return [self.currentBundle localizedStringForKey:tag value:tag table:nil];
}

@end
