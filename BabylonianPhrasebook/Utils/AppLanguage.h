//
//  AppLanguage.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 2/21/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <Foundation/Foundation.h>

#define BPCurrentLanguage() [[AppLanguage sharedInstance] currentLanguage]
#define BPLocalizedString(tag) [[AppLanguage sharedInstance] localizedStringForTag:tag]

#define kAddPhraseEmail @"info@stupidcasual.ru"

@interface AppLanguage : NSObject

+ (AppLanguage *)sharedInstance;

@property (nonatomic, strong) NSArray *languages;
@property (nonatomic, readonly) NSString *currentLanguage;
@property (nonatomic, readonly) NSString *systemPreferredLanguage;

@property (nonatomic, strong) NSString *currentTranslateLanguage;

- (void)changeCurrentLanguageToNext;

- (NSString *)localizedStringForTag:(NSString *)tag;

@end
