//
//  ImageCacheGenerator.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 2/19/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "ImageCacheGenerator.h"
#import "UIView+Ext.h"
#import <QuartzCore/QuartzCore.h>
#import "FavoriteBackgroundCellView.h"
#import "UIView+Sizes.h"
#import "PhraseBackgroundBubbleView.h"

@interface ImageCacheGenerator()

@end

@implementation ImageCacheGenerator

+ (ImageCacheGenerator *)sharedInstance
{
    static dispatch_once_t onceQueue;
    static ImageCacheGenerator *imageCacheGenerator = nil;
    
    dispatch_once(&onceQueue, ^{ imageCacheGenerator = [[self alloc] init]; });
    return imageCacheGenerator;
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    
    return self;
}

- (NSString *)phraseHeaderBackgroundFilenameWithHeight:(NSInteger)height {
    return [NSString stringWithFormat:@"phrase.header.background_%d@2x.png", height];
}

- (void)generateFavoriteCellsBackground {
    FavoriteBackgroundCellView *view = [[FavoriteBackgroundCellView alloc] initWithFrame:CGRectMake(0, 0, 320, 63)];
    view.drawType = 0;
    [self saveImage:[self pngImageFromView:view] withFilename:[self favoriteCellBackgroundFilenameWithType:view.drawType height:view.height]];
    view.drawType = 1;
    [self saveImage:[self pngImageFromView:view] withFilename:[self favoriteCellBackgroundFilenameWithType:view.drawType height:view.height]];
    view.drawType = 2;
    [self saveImage:[self pngImageFromView:view] withFilename:[self favoriteCellBackgroundFilenameWithType:view.drawType height:view.height]];
    
    CGFloat delta = 21;
    for (NSInteger i = 0; i < 14; i++) {
        CGFloat height = 67 + i * delta;
        view.height = height;
        
        view.drawType = 0;
        [self saveImage:[self pngImageFromView:view] withFilename:[self favoriteCellBackgroundFilenameWithType:view.drawType height:view.height]];
        view.drawType = 1;
        [self saveImage:[self pngImageFromView:view] withFilename:[self favoriteCellBackgroundFilenameWithType:view.drawType height:view.height]];
        view.drawType = 2;
        [self saveImage:[self pngImageFromView:view] withFilename:[self favoriteCellBackgroundFilenameWithType:view.drawType height:view.height]];
    }
}

- (NSString *)favoriteCellBackgroundFilenameWithType:(NSInteger) type height:(NSInteger)height {
    return [NSString stringWithFormat:@"fav.cell.background_%d_%d@2x.png", type, height];
}

- (UIImage *)pngImageFromView:(UIView *)view {
    view.backgroundColor = [UIColor clearColor];
    [view setNeedsDisplay];
    UIImage *image = [view renderToImage];
    NSData* pngdata = UIImagePNGRepresentation(image);
    UIImage* img = [UIImage imageWithData:pngdata];
    return img;
}

- (void)saveImage:(UIImage *)image withFilename:(NSString *)filename {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *filepath = [NSString stringWithFormat:@"/Users/opedge/fav_cells/%@", filename];
        NSData *imageData = UIImagePNGRepresentation(image);
        [imageData writeToFile:filepath atomically:YES];
        NSLog(@"Image saved: %@", filepath);
    });
}

- (void)generatePhraseBackground {
    CGFloat width;
    if (IS_IPAD()) {
        width = 430;
    } else {
        width = 280;
    }
    
    if (IS_IPAD()) {
        PhraseBackgroundBubbleView *view = [[PhraseBackgroundBubbleView alloc] initWithFrame:CGRectMake(0, 0, width, 0)];
        view.height = 100;
        [self saveImage:[self pngImageFromView:view] withFilename:[self phraseBackgroundFilenameWithType:0 height:view.height]];
        CGFloat delta = 30;
        for (NSInteger i = 0; i < 14; i++) {
            CGFloat height = 129 + i * delta;
            view.height = height;
        
            [self saveImage:[self pngImageFromView:view] withFilename:[self phraseBackgroundFilenameWithType:0 height:view.height]];
        }
    } else {
        PhraseBackgroundBubbleView *view = [[PhraseBackgroundBubbleView alloc] initWithFrame:CGRectMake(0, 0, width, 0)];
        CGFloat delta = 21;
        for (NSInteger i = 0; i < 15; i++) {
            CGFloat height = 61 + i * delta;
            view.height = height;
        
            [self saveImage:[self pngImageFromView:view] withFilename:[self phraseBackgroundFilenameWithType:0 height:view.height]];
        }
    }
}

- (NSString *)phraseBackgroundFilenameWithType:(NSInteger)type height:(NSInteger)height {
    return [NSString stringWithFormat:@"phrase.background_%d_%d@2x.png", type, height];
}

@end
