//
//  FavoriteBackgroundCellView.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 2/19/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "FavoriteBackgroundCellView.h"

@implementation FavoriteBackgroundCellView

- (void)drawRect:(CGRect)rect {
    switch (self.drawType) {
        case 0:
            [self drawRectType0:rect];
            break;
        case 1:
            [self drawRectType1:rect];
            break;
            
        case 2:
            [self drawRectType2:rect];
            break;
            
//        case 3:
//            [self drawRectType3:rect];
//            break;
            
        default:
            break;
    }
}

- (void)drawRectType0:(CGRect)rect {
    //// General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor* bottomGradientColor = [UIColor colorWithRed: 0.841 green: 0.841 blue: 0.841 alpha: 1];
    UIColor* strokeColor = [UIColor colorWithRed: 0.664 green: 0.653 blue: 0.625 alpha: 1];
    
    //// Gradient Declarations
    NSArray* cellGradientColors = [NSArray arrayWithObjects:
                                   (id)[UIColor whiteColor].CGColor,
                                   (id)[UIColor colorWithRed: 0.921 green: 0.921 blue: 0.921 alpha: 1].CGColor,
                                   (id)bottomGradientColor.CGColor, nil];
    CGFloat cellGradientLocations[] = {0, 0.63, 1};
    CGGradientRef cellGradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)cellGradientColors, cellGradientLocations);
    
    //// Rectangle Drawing
    UIBezierPath* rectanglePath = [UIBezierPath bezierPath];
    [rectanglePath moveToPoint: CGPointMake(-2, CGRectGetMaxY(rect) - 3.1)];
    [rectanglePath addCurveToPoint: CGPointMake(77.31, CGRectGetMaxY(rect) - 3.1) controlPoint1: CGPointMake(-2, CGRectGetMaxY(rect) - 3.1) controlPoint2: CGPointMake(1.32, CGRectGetMaxY(rect) + 0.98)];
    [rectanglePath addCurveToPoint: CGPointMake(145.01, CGRectGetMaxY(rect) - 1) controlPoint1: CGPointMake(89.61, CGRectGetMaxY(rect) - 3.76) controlPoint2: CGPointMake(118.57, CGRectGetMaxY(rect) - 1.21)];
    [rectanglePath addCurveToPoint: CGPointMake(228.73, CGRectGetMaxY(rect) - 3.1) controlPoint1: CGPointMake(161.33, CGRectGetMaxY(rect) - 0.87) controlPoint2: CGPointMake(218.75, CGRectGetMaxY(rect) - 3.45)];
    [rectanglePath addCurveToPoint: CGPointMake(322, CGRectGetMaxY(rect) - 1) controlPoint1: CGPointMake(268.65, CGRectGetMaxY(rect) - 1.71) controlPoint2: CGPointMake(322, CGRectGetMaxY(rect) - 1)];
    
    [rectanglePath addLineToPoint: CGPointMake(322, -2)];
    [rectanglePath addLineToPoint: CGPointMake(-2, -2)];
    [rectanglePath addLineToPoint: CGPointMake(-2, CGRectGetMaxY(rect) - 3.1)];
    [rectanglePath closePath];
    CGContextSaveGState(context);
    [rectanglePath addClip];
    CGContextDrawLinearGradient(context, cellGradient, CGPointMake(160, -2), CGPointMake(160, CGRectGetMaxY(rect) - 1), 0);
    CGContextRestoreGState(context);
    [strokeColor setStroke];
    rectanglePath.lineWidth = 2;
    [rectanglePath stroke];
    
    
    //// Cleanup
    CGGradientRelease(cellGradient);
    CGColorSpaceRelease(colorSpace);
}

- (void)drawRectType1:(CGRect)rect {
    //// General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor* bottomGradientColor = [UIColor colorWithRed: 0.841 green: 0.841 blue: 0.841 alpha: 1];
    UIColor* strokeColor = [UIColor colorWithRed: 0.664 green: 0.653 blue: 0.625 alpha: 1];
    
    //// Gradient Declarations
    NSArray* cellGradientColors = [NSArray arrayWithObjects:
                                   (id)[UIColor whiteColor].CGColor,
                                   (id)[UIColor colorWithRed: 0.921 green: 0.921 blue: 0.921 alpha: 1].CGColor,
                                   (id)bottomGradientColor.CGColor, nil];
    CGFloat cellGradientLocations[] = {0, 0.63, 1};
    CGGradientRef cellGradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)cellGradientColors, cellGradientLocations);
    
    //// Rectangle Drawing
    UIBezierPath* rectanglePath = [UIBezierPath bezierPath];
    [rectanglePath moveToPoint: CGPointMake(-2, CGRectGetMaxY(rect) - 1)];
    [rectanglePath addCurveToPoint: CGPointMake(193.47, CGRectGetMaxY(rect) - 3.1) controlPoint1: CGPointMake(-2, CGRectGetMaxY(rect) - 1) controlPoint2: CGPointMake(170.21, CGRectGetMaxY(rect) - 1.23)];
    [rectanglePath addCurveToPoint: CGPointMake(273.06, CGRectGetMaxY(rect) - 3.1) controlPoint1: CGPointMake(207.82, CGRectGetMaxY(rect) - 4.25) controlPoint2: CGPointMake(263.08, CGRectGetMaxY(rect) - 3.45)];
    [rectanglePath addCurveToPoint: CGPointMake(322, CGRectGetMaxY(rect) - 1) controlPoint1: CGPointMake(312.98, CGRectGetMaxY(rect) - 1.71) controlPoint2: CGPointMake(322, CGRectGetMaxY(rect) - 1)];
    [rectanglePath addLineToPoint: CGPointMake(322, -2)];
    [rectanglePath addLineToPoint: CGPointMake(-2, -2)];
    [rectanglePath addLineToPoint: CGPointMake(-2, CGRectGetMaxY(rect) - 1)];
    [rectanglePath closePath];
    CGContextSaveGState(context);
    [rectanglePath addClip];
    CGContextDrawLinearGradient(context, cellGradient, CGPointMake(160, -2), CGPointMake(160, CGRectGetMaxY(rect) - 1), 0);
    CGContextRestoreGState(context);
    [strokeColor setStroke];
    rectanglePath.lineWidth = 2;
    [rectanglePath stroke];
    
    
    //// Cleanup
    CGGradientRelease(cellGradient);
    CGColorSpaceRelease(colorSpace);
}

- (void)drawRectType2:(CGRect)rect {
    //// General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor* bottomGradientColor = [UIColor colorWithRed: 0.841 green: 0.841 blue: 0.841 alpha: 1];
    UIColor* strokeColor = [UIColor colorWithRed: 0.664 green: 0.653 blue: 0.625 alpha: 1];
    
    //// Gradient Declarations
    NSArray* cellGradientColors = [NSArray arrayWithObjects:
                                   (id)[UIColor whiteColor].CGColor,
                                   (id)[UIColor colorWithRed: 0.921 green: 0.921 blue: 0.921 alpha: 1].CGColor,
                                   (id)bottomGradientColor.CGColor, nil];
    CGFloat cellGradientLocations[] = {0, 0.63, 1};
    CGGradientRef cellGradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)cellGradientColors, cellGradientLocations);
    
    //// Rectangle Drawing
    UIBezierPath* rectanglePath = [UIBezierPath bezierPath];
    [rectanglePath moveToPoint: CGPointMake(-2, CGRectGetMaxY(rect) - 2.25)];
    [rectanglePath addCurveToPoint: CGPointMake(77.8, CGRectGetMaxY(rect) - 2.25) controlPoint1: CGPointMake(-2, CGRectGetMaxY(rect) - 2.25) controlPoint2: CGPointMake(-1.26, CGRectGetMaxY(rect) - 4.91)];
    [rectanglePath addCurveToPoint: CGPointMake(117.5, CGRectGetMaxY(rect) - 2.25) controlPoint1: CGPointMake(98.48, CGRectGetMaxY(rect) - 1.43) controlPoint2: CGPointMake(89.99, CGRectGetMaxY(rect) - 2.32)];
    [rectanglePath addCurveToPoint: CGPointMake(324, CGRectGetMaxY(rect) - 1) controlPoint1: CGPointMake(195.15, CGRectGetMaxY(rect) - 1.66) controlPoint2: CGPointMake(324, CGRectGetMaxY(rect) - 1)];
    [rectanglePath addLineToPoint: CGPointMake(324, -2)];
    [rectanglePath addLineToPoint: CGPointMake(-2, -2)];
    [rectanglePath addLineToPoint: CGPointMake(-2, CGRectGetMaxY(rect) - 2.25)];
    [rectanglePath closePath];
    CGContextSaveGState(context);
    [rectanglePath addClip];
    CGContextDrawLinearGradient(context, cellGradient, CGPointMake(161, -2), CGPointMake(161, CGRectGetMaxY(rect) - 1), 0);
    CGContextRestoreGState(context);
    [strokeColor setStroke];
    rectanglePath.lineWidth = 2;
    [rectanglePath stroke];
    
    
    //// Cleanup
    CGGradientRelease(cellGradient);
    CGColorSpaceRelease(colorSpace);
}

//- (void)drawRectType3:(CGRect)rect {
//    //// General Declarations
//    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    
//    //// Color Declarations
//    UIColor* bottomGradientColor = [UIColor colorWithRed: 0.841 green: 0.841 blue: 0.841 alpha: 1];
//    UIColor* strokeColor = [UIColor colorWithRed: 0.664 green: 0.653 blue: 0.625 alpha: 1];
//    
//    //// Gradient Declarations
//    NSArray* cellGradientColors = [NSArray arrayWithObjects:
//                                   (id)[UIColor whiteColor].CGColor,
//                                   (id)[UIColor colorWithRed: 0.921 green: 0.921 blue: 0.921 alpha: 1].CGColor,
//                                   (id)bottomGradientColor.CGColor, nil];
//    CGFloat cellGradientLocations[] = {0, 0.63, 1};
//    CGGradientRef cellGradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)cellGradientColors, cellGradientLocations);
//    
//    //// Rectangle Drawing
//    UIBezierPath* rectanglePath = [UIBezierPath bezierPath];
//    [rectanglePath moveToPoint: CGPointMake(-2, CGRectGetMaxY(rect) - 2)];
//    [rectanglePath addCurveToPoint: CGPointMake(75.49, CGRectGetMaxY(rect) - 2.25) controlPoint1: CGPointMake(-2, CGRectGetMaxY(rect) - 2) controlPoint2: CGPointMake(34.33, CGRectGetMaxY(rect) - 0.54)];
//    [rectanglePath addCurveToPoint: CGPointMake(117.5, CGRectGetMaxY(rect) - 2.25) controlPoint1: CGPointMake(86.26, CGRectGetMaxY(rect) - 2.57) controlPoint2: CGPointMake(89.99, CGRectGetMaxY(rect) - 2.32)];
//    [rectanglePath addCurveToPoint: CGPointMake(192.91, CGRectGetMaxY(rect) - 2.15) controlPoint1: CGPointMake(133.67, CGRectGetMaxY(rect) - 2.05) controlPoint2: CGPointMake(169.76, CGRectGetMaxY(rect) - 0.75)];
//    [rectanglePath addCurveToPoint: CGPointMake(311.69, CGRectGetMaxY(rect) - 2.25) controlPoint1: CGPointMake(217.21, CGRectGetMaxY(rect) - 2.57) controlPoint2: CGPointMake(306.57, CGRectGetMaxY(rect) - 2.68)];
//    [rectanglePath addCurveToPoint: CGPointMake(324, CGRectGetMaxY(rect) - 1) controlPoint1: CGPointMake(325.09, CGRectGetMaxY(rect) - 0.76) controlPoint2: CGPointMake(324, CGRectGetMaxY(rect) - 1)];
//    [rectanglePath addLineToPoint: CGPointMake(324, -2)];
//    [rectanglePath addLineToPoint: CGPointMake(-2, -2)];
//    [rectanglePath addLineToPoint: CGPointMake(-2, CGRectGetMaxY(rect) - 2)];
//    [rectanglePath closePath];
//    CGContextSaveGState(context);
//    [rectanglePath addClip];
//    CGContextDrawLinearGradient(context, cellGradient, CGPointMake(161.01, -2), CGPointMake(161.01, CGRectGetMaxY(rect) - 0.98), 0);
//    CGContextRestoreGState(context);
//    [strokeColor setStroke];
//    rectanglePath.lineWidth = 2;
//    [rectanglePath stroke];
//    
//    
//    //// Cleanup
//    CGGradientRelease(cellGradient);
//    CGColorSpaceRelease(colorSpace);
//}

- (void)setDrawType:(NSInteger)drawType {
    if (_drawType != drawType) {
        _drawType = drawType;
    }
    [self setNeedsDisplay];
}


@end
