//
//  PhraseViewNew.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 2/21/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"
#import "LangPhrase.h"
#import "Phrase.h"

@class PhraseView;

@protocol PhraseViewDelegate <NSObject>

@optional
- (void)phraseView:(PhraseView *)phraseView didClickedShareButton:(UIButton *)shareButton withPhrase:(Phrase *)phrase withMainLangPhrase:(LangPhrase *)langPhrase;

@end

@interface PhraseView : UIView <SwipeItemView>

@property (nonatomic, strong) NSString *reuseIdentifier;

@property (nonatomic, weak) id<PhraseViewDelegate> delegate;

- (void)populateWithMainLangPhrase:(LangPhrase *)mainLangPhrase secondLangPhrase:(LangPhrase *)secondLangPhrase isFavorite:(BOOL)isFavorite;

- (void)scrollToTop;

- (id)initWithFrame:(CGRect)frame bottomMargin:(CGFloat)bottomMargin;

@end
