//
//  UIView+Ext.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 2/19/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "UIView+Ext.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (Ext)

- (UIImage*) renderToImage
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    
    CGContextTranslateCTM(ctx, [self center].x, [self center].y);
    
    CGContextConcatCTM(ctx, [self transform]);
    
    CGContextTranslateCTM(ctx,
                          -[self bounds].size.width * [[self layer] anchorPoint].x,
                          -[self bounds].size.height * [[self layer] anchorPoint].y);
    
    [self.layer renderInContext:ctx];
    
    CGContextRestoreGState(ctx);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();
    return image;
}

@end
