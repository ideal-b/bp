//
//  BubbleView.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 03.04.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "SimpleBubbleView.h"
#import "PhraseBackgroundBubbleView.h"
#import "LangPhrase.h"
#import "UIView+Sizes.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+Size.h"

@interface SimpleBubbleView() <TTTAttributedLabelDelegate> {
    BOOL _isMain;
    BOOL _isSelectAll;
    NSRange _textRange;
    
    CGFloat _fontSize;
    
    CGFloat _backWidth;
    CGFloat _minHeightMain;
    CGFloat _minHeightBottom;
    
    CGSize _favButtonSize;
    
    CGFloat _langLabelLeftMargin;
    CGFloat _langLabelTopMargin;
    CGFloat _langLabelRightMargin;
    CGFloat _langLabelBottomMargin;
}

@property (nonatomic, weak) PhraseBackgroundBubbleView *backView;
@property (nonatomic, weak) UIImageView *backImageView;

@property (nonatomic, weak) UIImageView *bubblePartImageView;
@property (nonatomic, weak) UIImageView *faceImageView;

@end

@implementation SimpleBubbleView

- (id)initAsMain:(BOOL)isMain
{    
    if (IS_IPAD()) {
        _backWidth = 430;
        _minHeightMain = 129;
        _minHeightBottom = 100;
        _fontSize = 24;
        _favButtonSize = CGSizeMake(70, 70);
        
        _langLabelLeftMargin = 14;
        _langLabelTopMargin = 20;
        _langLabelRightMargin = 20;
        _langLabelBottomMargin = 20;
    } else {
        _backWidth = 280;
        _minHeightMain = 82;
        _minHeightBottom = 61;
        _fontSize = 17;
        _favButtonSize = CGSizeMake(58, 44);
        
        _langLabelLeftMargin = 10;
        _langLabelTopMargin = 10;
        _langLabelRightMargin = 10;
        _langLabelBottomMargin = 10;
    }
    
    self = [super initWithFrame:CGRectMake(0, 0, _backWidth, 0)];
    if (self) {
        _isMain = isMain;
        PhraseBackgroundBubbleView *backgroundBubbleView = [[PhraseBackgroundBubbleView alloc] initWithFrame:CGRectZero];
        backgroundBubbleView.backgroundColor = [UIColor clearColor];
        backgroundBubbleView.hidden = YES;
        [self addSubview:backgroundBubbleView];
        self.backView = backgroundBubbleView;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _backWidth, 0)];
        [self addSubview:imageView];
        self.backImageView = imageView;
        
        CGFloat langLableWidth = _backWidth - 12;
        if (isMain) {
            langLableWidth -= _favButtonSize.width - 7;
            UIButton *favoriteButton = [UIButton buttonWithType:UIButtonTypeCustom];
            favoriteButton.frame = CGRectMake(_backWidth - _favButtonSize.width - 5, -5, _favButtonSize.width, _favButtonSize.height);
            UIImage *buttonImage = [self favoriteButtonImage:NO];
            UIImage *buttonImageHl = [self favoriteButtonImage:YES];
            [favoriteButton setImage:buttonImage forState:UIControlStateNormal];
            [favoriteButton setImage:buttonImageHl forState:UIControlStateSelected];
            [favoriteButton setImage:buttonImageHl forState:UIControlStateHighlighted];
            [favoriteButton setImage:buttonImageHl forState:(UIControlStateSelected|UIControlStateHighlighted)];
            
//            favoriteButton.layer.borderColor = [UIColor blackColor].CGColor;
//            favoriteButton.layer.borderWidth = 1;
            favoriteButton.imageEdgeInsets = UIEdgeInsetsMake(10, 0, 0, 0);
            
            [self addSubview:favoriteButton];
            self.favoriteButton = favoriteButton;
            
            UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
            shareButton.frame = CGRectMake(favoriteButton.origin.x, favoriteButton.bottom, favoriteButton.width, 44);
            [shareButton setImage:[UIImage imageNamed:@"button.share.png"] forState:UIControlStateNormal];
            [shareButton setImage:[UIImage imageNamed:@"button.share.hl.png"] forState:UIControlStateHighlighted];
            shareButton.imageEdgeInsets = UIEdgeInsetsMake(-8, 0, 0, 0);
            [self addSubview:shareButton];
            
            self.shareButton = shareButton;
                        
            UIImage *youImage = [UIImage imageNamed:[NSString stringWithFormat:@"face_%@.png", BPCurrentLanguage()]];
            
            UIImageView *faceImageView = [[UIImageView alloc] initWithImage:youImage];
            faceImageView.transform = CGAffineTransformMake(faceImageView.transform.a * -1, 0, 0, 1, faceImageView.transform.tx, 0);
            faceImageView.origin = CGPointMake(10, 0);
            [self addSubview:faceImageView];
            self.faceImageView = faceImageView;
            
            UIImage *bubblePartImage = [self bubblePartImage:YES];
            UIImageView *bubblePartImageView = [[UIImageView alloc] initWithFrame:CGRectMake(faceImageView.right + 5, 0, bubblePartImage.size.width, bubblePartImage.size.height)];
            bubblePartImageView.image = bubblePartImage;
            [self addSubview:bubblePartImageView];
            self.bubblePartImageView = bubblePartImageView;            
        } else {
            UIImageView *faceImageView = [[UIImageView alloc] init];
            [self addSubview:faceImageView];
            self.faceImageView = faceImageView;
            
            UIImage *bubblePartImage = [self bubblePartImage:NO];
            UIImageView *bubblePartImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_backWidth - 64 - 19, 0, bubblePartImage.size.width, bubblePartImage.size.height)];
            bubblePartImageView.image = bubblePartImage;
            [self addSubview:bubblePartImageView];
            self.bubblePartImageView = bubblePartImageView;
        }   
        
        TTTAttributedLabel *langLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(_langLabelLeftMargin, _langLabelTopMargin, langLableWidth - _langLabelRightMargin, 0)];
        langLabel.delegate = self;
        langLabel.textInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        langLabel.dataDetectorTypes = UIDataDetectorTypeNone;
        langLabel.backgroundColor = [UIColor clearColor];
        langLabel.font = [UIFont systemFontOfSize:_fontSize];
        langLabel.textColor = [UIColor blackColor];
        langLabel.numberOfLines = 0;
        
//        langLabel.layer.borderColor = [UIColor redColor].CGColor;
//        langLabel.layer.borderWidth = 1;
        
        [self addSubview:langLabel];
        self.langLabel = langLabel;
    }
    return self;
}

- (UIImage *)favoriteButtonImage:(BOOL)isHightlighted {    
    if (!isHightlighted) {
        UIImage *buttonImage = [UIImage imageNamed:@"fav.button.png"];
        return buttonImage;
    } else {
        UIImage *buttonImageHl = [UIImage imageNamed:@"fav.button.hl.png"];
        return buttonImageHl;
    }
}

- (UIImage *)bubblePartImage:(BOOL)isLeft {
    UIImage *res = isLeft ? [UIImage imageNamed:@"phrase.bubble.part.png"] : [UIImage imageNamed:@"phrase.bubble.part2.png"];
    return res;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIMenuControllerWillHideMenuNotification
                                                  object:nil];
}

- (void)populateWithLangPhrase:(LangPhrase *)langPhrase {
    NSString *currentLang = BPCurrentLanguage();
    if ([currentLang isEqualToString:@"ru"] && !_isMain) {
        NSString *text = [NSString stringWithFormat:@"%@\n%@", langPhrase.text, langPhrase.trans_ru];
        
        NSRange transRange = NSMakeRange(langPhrase.text.length + 1, langPhrase.trans_ru.length);
        
        [self.langLabel setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
            UIFont *italicSystemFont = [UIFont italicSystemFontOfSize:_fontSize];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)italicSystemFont.fontName, italicSystemFont.pointSize, NULL);
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:transRange];
            [mutableAttributedString addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[UIColor grayColor].CGColor range:transRange];
            
            return mutableAttributedString;
        }];
    } else {
        NSString *langPhraseText = langPhrase.text;
        [self.langLabel setText:langPhraseText];
    }
    
    CGFloat labelHeight = [self.langLabel sizeThatFits:CGSizeMake(self.langLabel.width, 9999)].height;
    self.langLabel.height = labelHeight;
    
    CGFloat h = self.langLabel.bottom + _langLabelBottomMargin;
    
    CGFloat height;
    if (_isMain) {
        height = MAX(_minHeightMain, h);
    } else {
        height = MAX(_minHeightBottom, h);
    }

    self.height = height;
    
    CGFloat backViewHeight = height;
    
    UIImage *backgroundImage = [UIImage imageNamed:[NSString stringWithFormat:@"phrase.background_0_%d.png", (NSInteger)backViewHeight]];
    if (backgroundImage) {
        self.backView.hidden = YES;
        self.backImageView.hidden = NO;
        self.backImageView.height = backViewHeight;
        self.backImageView.image = backgroundImage;
        self.backView.frame = CGRectZero;
    } else {
        self.backView.hidden = NO;
        self.backImageView.hidden = YES;
        self.backView.frame = CGRectMake(0, 0, _backWidth, backViewHeight);
    }
    
    if (!_isMain) {
        UIImage *faceImage = [self faceImageForLanguage:langPhrase.lang];
        self.faceImageView.frame = CGRectMake(_backWidth - 15 - faceImage.size.width, backViewHeight + 6, faceImage.size.width, faceImage.size.height);
        self.faceImageView.image = faceImage;
        self.bubblePartImageView.origin = CGPointMake(self.faceImageView.origin.x - self.bubblePartImageView.width - 5, self.bubblePartImageView.origin.y);
    }
    
    CGFloat offsetY = 4;
    if (IS_IPAD()) {
        offsetY = 5;
    }
    
    self.bubblePartImageView.origin = CGPointMake(self.bubblePartImageView.origin.x, backViewHeight - offsetY);
    self.faceImageView.origin = CGPointMake(self.faceImageView.origin.x, backViewHeight + 6);
    
    self.height = self.faceImageView.bottom;
}

- (UIImage *)faceImageForLanguage:(NSString *)language {
    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"face_%@.png", language]];
    if (!image) {
        image = [UIImage imageNamed:@"face_default.png"];
    }
    
    return image;
}

- (void)deselectAll {
    [self resignFirstResponder];
    [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];
    [self.langLabel deselectAll];
}

#pragma mark - TTTAttributedLabelDelegate
- (void)attributedLabel:(TTTAttributedLabel *)label didSelectWordAtRange:(NSRange)range inRect:(CGRect)rect {
    _textRange = range;
    _isSelectAll = NO;
    [self becomeFirstResponder];
    UIMenuController *menu = [UIMenuController sharedMenuController];    
    CGRect targetRect = [label convertRect:rect toView:self.superview];
    
    [menu setTargetRect:targetRect inView:self.superview];
    [menu setMenuVisible:YES animated:YES];
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(defineSelection:))
    {
        return NO;
    }
    else if (action == @selector(translateSelection:))
    {
        return NO;
    }
    else if (action == @selector(copy:))
    {
        return YES;
    }
    else if (action == @selector(selectAll:))
    {
        return !_isSelectAll;
    }
    
    return [super canPerformAction:action withSender:sender];
}

- (BOOL) canBecomeFirstResponder
{
    return YES;
}

- (void)selectAll:(id)sender {
    _isSelectAll = YES;
    [self.langLabel selectAllText];
    
    UIMenuController *menu = [UIMenuController sharedMenuController];
    [menu setTargetRect: [self convertRect:self.langLabel.frame toView:self.superview] inView:self.superview];
    [menu setMenuVisible:YES animated:YES];
}

- (void)copy:(id)sender {
    NSString *str;
    if (_isSelectAll) {
        str = self.langLabel.text;
    } else {
        NSString *txt = self.langLabel.text;
        str = [txt substringWithRange:_textRange];
    }
    
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    [pb setString:str];
    
    _isSelectAll = NO;
    [self.langLabel deselectAll];
    
}

@end
