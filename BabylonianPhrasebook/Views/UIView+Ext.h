//
//  UIView+Ext.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 2/19/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Ext)

- (UIImage*) renderToImage;

@end
