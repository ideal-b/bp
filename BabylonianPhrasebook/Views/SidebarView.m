//
//  SidebarView.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 03.04.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "SidebarView.h"
#import "UIView+Sizes.h"
#import <QuartzCore/QuartzCore.h>

@interface SidebarView() {
    BOOL _isMoving;
}

@property (nonatomic, strong) UIBezierPath *notClickablePath;
@property (nonatomic, strong) UIBezierPath *fullPath;

@end

@implementation SidebarView

- (void)awakeFromNib {
    _isMoving = NO;
    self.fullPath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, self.width, self.height)];
    self.notClickablePath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, 48, 204)];
    
    self.origin = CGPointMake(self.origin.x + self.width - 48, self.origin.y);
    UIPanGestureRecognizer *panRecongnizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    panRecongnizer.cancelsTouchesInView = NO;
    [self addGestureRecognizer:panRecongnizer];
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    CGPoint translation = [recognizer translationInView:self];
    
    if (translation.x < 0) {
        if (self.origin.x > self.superview.width - self.width) {
         
            self.center = CGPointMake(self.center.x + translation.x, self.center.y);
            if (self.origin.x < self.superview.width - self.width) {
                self.origin = CGPointMake(self.superview.width - self.width, self.origin.y);
            }
        }
    } else if (translation.x > 0) {
        if (self.origin.x < self.superview.width - 48) {
            self.center = CGPointMake(self.center.x + translation.x, self.center.y);
            if (self.origin.x > self.superview.width - 48) {
                self.origin = CGPointMake(self.superview.width - 48, self.origin.y);
            }
        }
    }
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:self];
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        _isMoving = YES;
        CGPoint velocity = [recognizer velocityInView:self];
        [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{            
            if (velocity.x < 0) {
                self.origin = CGPointMake(self.superview.width - self.width, self.origin.y);
                
                [Flurry logEvent:@"Options" withParameters:@{ @"Credits": @"info page opened" }];
            } else if (velocity.x > 0) {
                self.origin = CGPointMake(self.superview.width - 48, self.origin.y);
            }
        } completion:^(BOOL finished) {
            if (velocity.x < 0) {
                [self.button setImage:[UIImage imageNamed:@"sidebar.arrow2.png"] forState:UIControlStateNormal];
            } else {
                [self.button setImage:[UIImage imageNamed:@"sidebar.arrow.png"] forState:UIControlStateNormal];
            }
            _isMoving = !finished;
        }];
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    CGPoint p = [self convertPoint:point toView:self];
    
    if (![self.notClickablePath containsPoint:p] && [self.fullPath containsPoint:p]) {
        
        for (UIView *child in self.subviews) {
            if ([child isKindOfClass:[UIButton class]]) {
                UIButton *b = (UIButton *)child;
                if (CGRectContainsPoint(b.frame, p)) {
                    return b;
                }
            }
        }
        
        return self;
    }
    
    return nil;
}

- (void)showSidebar {
    if (!_isMoving) {
        [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.origin = CGPointMake(self.superview.width - self.width, self.origin.y);
        } completion:^(BOOL finished) {
            [self.button setImage:[UIImage imageNamed:@"sidebar.arrow2.png"] forState:UIControlStateNormal];
        }];
        
        [Flurry logEvent:@"Options" withParameters:@{ @"Credits": @"info page opened" }];
    }
}

- (void)hideSidebar {
    if (!_isMoving) {
        [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.origin = CGPointMake(self.superview.width - 48, self.origin.y);
        } completion:^(BOOL finished) {
            [self.button setImage:[UIImage imageNamed:@"sidebar.arrow.png"] forState:UIControlStateNormal];            
        }];
    }
}

@end
