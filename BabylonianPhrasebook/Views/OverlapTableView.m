//
//  FavoritesTableView.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 03.02.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "OverlapTableView.h"

@implementation OverlapTableView

- (void)layoutSubviews {
    [super layoutSubviews];
    
    NSArray *sortedIndexPaths = [[self indexPathsForVisibleRows] sortedArrayUsingSelector:@selector(compare:)];
    for (NSIndexPath *path in sortedIndexPaths) {
        UITableViewCell *cell = [self cellForRowAtIndexPath:path];
        [self sendSubviewToBack:cell];
    }
}

@end
