//
//  FavoriteBackgroundCellView.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 2/19/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteBackgroundCellView : UIView

@property (nonatomic, assign) NSInteger drawType;

@end
