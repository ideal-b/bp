//
//  SidebarView.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 03.04.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SidebarView : UIView

- (void)showSidebar;

- (void)hideSidebar;

@property (nonatomic, weak) IBOutlet UIButton *button;

@end
