//
//  PhraseBackgroundCellView.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 2/19/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "PhraseBackgroundBubbleView.h"
#import "UIColor+RGB.h"

@implementation PhraseBackgroundBubbleView

- (void)drawRect:(CGRect)rect {
    [self drawRectType0:rect];
}

- (void)drawRectType0:(CGRect)rect {
    //// General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor* strokeColor = [UIColor colorWithRed: 0.661 green: 0.649 blue: 0.621 alpha: 1];
    UIColor* cellGradientColor1 = [UIColor colorWithRGBHexString:@"d9d7d5"];
    
    //// Gradient Declarations
    NSArray* cellGradientColors = [NSArray arrayWithObjects:
                                   (id)[UIColor whiteColor].CGColor,
                                   (id)[UIColor colorWithRed: 0.941 green: 0.937 blue: 0.934 alpha: 1].CGColor,
                                   (id)cellGradientColor1.CGColor, nil];
    CGFloat cellGradientLocations[] = {0, 0.48, 1};
    CGGradientRef cellGradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)cellGradientColors, cellGradientLocations);
    
    //// Frames
    CGRect frame = CGRectMake(0, 0, CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    
    
    //// Rectangle Drawing
    UIBezierPath* rectanglePath = [UIBezierPath bezierPath];
    [rectanglePath moveToPoint: CGPointMake(CGRectGetMinX(frame) + 4, CGRectGetMinY(frame) + 6.99)];
    [rectanglePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 4, CGRectGetMinY(frame) + 32.88) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 3.75, CGRectGetMinY(frame) + 10.87) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 3.22, CGRectGetMinY(frame) + 18.06)];
    [rectanglePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 4.34, CGRectGetMaxY(frame) - 8.28) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 4.78, CGRectGetMinY(frame) + 47.78) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 4.6, CGRectGetMaxY(frame) - 13.08)];
    [rectanglePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 7.15, CGRectGetMaxY(frame) - 2.95) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 4.18, CGRectGetMaxY(frame) - 5.31) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 2.92, CGRectGetMaxY(frame) - 3.46)];
    [rectanglePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.28686 * CGRectGetWidth(frame), CGRectGetMaxY(frame) - 3) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 14.91, CGRectGetMaxY(frame) - 2.02) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.12740 * CGRectGetWidth(frame), CGRectGetMaxY(frame) - 4.07)];
    [rectanglePath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 8.71, CGRectGetMaxY(frame) - 3.93) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.50834 * CGRectGetWidth(frame), CGRectGetMaxY(frame) - 1.51) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 30.02, CGRectGetMaxY(frame) - 3.5)];
    [rectanglePath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 5.39, CGRectGetMaxY(frame) - 12.31) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 6.3, CGRectGetMaxY(frame) - 3.97) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 5.22, CGRectGetMaxY(frame) - 7.02)];
    [rectanglePath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 5.33, CGRectGetMinY(frame) + 0.42612 * CGRectGetHeight(frame)) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 5.6, CGRectGetMaxY(frame) - 18.72) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 5.74, CGRectGetMinY(frame) + 0.59359 * CGRectGetHeight(frame))];
    [rectanglePath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 5.3, CGRectGetMinY(frame) + 5.82) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 4.86, CGRectGetMinY(frame) + 0.23451 * CGRectGetHeight(frame)) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 5.07, CGRectGetMinY(frame) + 9.28)];
    [rectanglePath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 12.38, CGRectGetMinY(frame) + 3.05) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 5.43, CGRectGetMinY(frame) + 3.76) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 8.01, CGRectGetMinY(frame) + 3.02)];
    [rectanglePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.25869 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 3) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 42.47, CGRectGetMinY(frame) + 3.25) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 0.41680 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 3.97)];
    [rectanglePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 19.35, CGRectGetMinY(frame) + 3) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 0.07761 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 1.89) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 19.35, CGRectGetMinY(frame) + 3)];
    [rectanglePath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 9.63, CGRectGetMinY(frame) + 3)];
    [rectanglePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 4, CGRectGetMinY(frame) + 6.99) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 9.63, CGRectGetMinY(frame) + 3) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 4.16, CGRectGetMinY(frame) + 4.56)];
    [rectanglePath closePath];
    CGContextSaveGState(context);
    [rectanglePath addClip];
    CGRect rectangleBounds = rectanglePath.bounds;
    CGContextDrawLinearGradient(context, cellGradient,
                                CGPointMake(CGRectGetMidX(rectangleBounds), CGRectGetMinY(rectangleBounds)),
                                CGPointMake(CGRectGetMidX(rectangleBounds), CGRectGetMaxY(rectangleBounds)),
                                0);
    CGContextRestoreGState(context);
    [strokeColor setStroke];
    rectanglePath.lineWidth = 1.5;
    [rectanglePath stroke];
    
    
    //// Cleanup
    CGGradientRelease(cellGradient);
    CGColorSpaceRelease(colorSpace);
}

@end
