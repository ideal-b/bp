//
//  CategoryCell.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 30.03.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "CategoryCell.h"
#import "UIColor+RGB.h"
#import "UIView+Sizes.h"
#import <QuartzCore/QuartzCore.h>

@interface CategoryCell()

@property (nonatomic, weak) UIView *backgroundContainerView;
@property (nonatomic, weak) UIImageView *backgroundImageView;
@property (nonatomic, weak) UILabel *categoryNameLabel;
@property (nonatomic, weak) UIView *colorView;

@property (nonatomic, strong) NSArray *cellColors;

@end

@implementation CategoryCell

- (void)awakeFromNib {
    self.cellColors = @[
                        [UIColor colorWithRGBHexString:@"008cf0"],
                        [UIColor colorWithRGBHexString:@"02b01b"],
                        [UIColor colorWithRGBHexString:@"ba3232"],
                        [UIColor colorWithRGBHexString:@"ffae58"],
                        [UIColor colorWithRGBHexString:@"964b00"],
                        [UIColor colorWithRGBHexString:@"013220"],
                        [UIColor colorWithRGBHexString:@"c8a2c8"],
                        [UIColor colorWithRGBHexString:@"ffa500"],
                        [UIColor colorWithRGBHexString:@"3c65a8"],
                        [UIColor colorWithRGBHexString:@"8b00ff"],
                        [UIColor colorWithRGBHexString:@"000000"]
                        ];
    
    self.clipsToBounds = NO;
    self.contentView.clipsToBounds = NO;
    self.contentView.backgroundColor = [UIColor clearColor];
    
    UIView *backgroundContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0)];
    backgroundContainerView.clipsToBounds = NO;
    backgroundContainerView.backgroundColor = [UIColor clearColor];
    
    [self.contentView addSubview:backgroundContainerView];
    self.backgroundContainerView = backgroundContainerView;
    
    UIView *colorView = [[UIView alloc] initWithFrame:CGRectMake(0, -4, 12, 0)];
    [self.backgroundContainerView addSubview:colorView];
    self.colorView = colorView;
    
    UIImageView *backgroundImageView = [[UIImageView alloc] init];
    [self.backgroundContainerView addSubview:backgroundImageView];
    self.backgroundImageView = backgroundImageView;
    
    UILabel *categoryNameLabel = [[UILabel alloc] init];
    categoryNameLabel.numberOfLines = 1;
    categoryNameLabel.backgroundColor = [UIColor clearColor];
    
    if (IS_IPAD()) {
        categoryNameLabel.frame = CGRectMake(24, 0, 260, 0);
        categoryNameLabel.textAlignment = NSTextAlignmentLeft;
    } else {
        categoryNameLabel.frame = CGRectMake(0, 0, 320, 0);
        categoryNameLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    categoryNameLabel.textColor = [UIColor colorWithRGBHexString:@"4b868f"];
    categoryNameLabel.font = [UIFont fontWithName:@"DR Agu" size:18];
    
    [self.backgroundContainerView addSubview:categoryNameLabel];
    self.categoryNameLabel = categoryNameLabel;
}

- (void)populateWithCategory:(Category *)category forIndexPath:(NSIndexPath *)indexPath {
    
    NSString *categoryCellBackgroundName = nil;
    NSInteger index = indexPath.row % 3;
    switch (index) {
        case 0:
            categoryCellBackgroundName = @"category_cell_01.png";
            break;
        case 1:
            categoryCellBackgroundName = @"category_cell_02.png";
            break;
        case 2:
            categoryCellBackgroundName = @"category_cell_03.png";
            break;
    }
    
    UIImage *cellBackgroundImage = [UIImage imageNamed:categoryCellBackgroundName];
    
    CGFloat height = [CategoryCell heightForIndexPath:indexPath];

    self.colorView.frame = [CategoryCell frameForColorViewAtIndexPath:indexPath];
    self.colorView.backgroundColor = [self.cellColors objectAtIndex:(indexPath.row % [self.cellColors count])];
    
    CGRect frame = CGRectMake(0, 0, 320, height + 3);
    self.categoryNameLabel.height = height;
    self.categoryNameLabel.text = category.name;
    [self.backgroundContainerView setFrame:frame];
    [self.backgroundImageView setFrame:CGRectMake(0, 0, 320, height + 3)];
    [self.backgroundImageView setImage:cellBackgroundImage];
}

+ (CGFloat)heightForIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = indexPath.row % 3;
    
    CGFloat result = 50;
    switch (index) {
        case 0:
            result = 50;
            break;
        case 1:
            result = 57;
            break;
        case 2:
            result = 57;
            break;
    }
    
    return result;
}

+ (CGRect)frameForColorViewAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = indexPath.row % 3;
    
    CGRect rect;
    switch (index) {
        case 0:
            rect = CGRectMake(0, 0, 14, [CategoryCell heightForIndexPath:indexPath] );
            break;
            
        case 1:
            rect = CGRectMake(0, 0, 14, [CategoryCell heightForIndexPath:indexPath]);
            break;
            
        case 2:
            rect = CGRectMake(0, 0, 14, [CategoryCell heightForIndexPath:indexPath] + 1);
            break;
            
        default:
            rect = CGRectZero;
            break;
    }
    
    return rect;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if (IS_IPAD()) {
        if (!selected) {
            self.categoryNameLabel.textColor = [UIColor colorWithRGBHexString:@"4b868f"];
        } else {
            self.categoryNameLabel.textColor = [UIColor colorWithRGBHexString:@"923f2f"];
        }
    }
    
}

@end
