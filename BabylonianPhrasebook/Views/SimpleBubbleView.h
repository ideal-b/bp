//
//  BubbleView.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 03.04.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@class LangPhrase;

@interface SimpleBubbleView : UIView

@property (nonatomic, weak) TTTAttributedLabel *langLabel;
@property (nonatomic, weak) UIButton *favoriteButton;
@property (nonatomic, weak) UIButton *shareButton;

- (void)populateWithLangPhrase:(LangPhrase *)langPhrase;

- (id)initAsMain:(BOOL)isMain;

- (void)deselectAll;

@end
