//
//  PhraseViewNew.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 2/21/13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "PhraseView.h"
#import "UILabel+Size.h"
#import "UIView+Sizes.h"
#import "PhraseBackgroundBubbleView.h"
#import "Phrase.h"
#import "SimpleBubbleView.h"
#import <QuartzCore/QuartzCore.h>
#import "TTTAttributedLabel.h"

@interface PhraseView() <UIScrollViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, weak) UIScrollView *scrollView;

@property (nonatomic, weak) SimpleBubbleView *topBubbleView;

@property (nonatomic, weak) SimpleBubbleView *bottomBubbleView;

@property (nonatomic, weak) UIImageView *backPanelView;
@property (nonatomic, assign) CGFloat backPanelViewHeight;

@property (nonatomic, strong) Phrase *phrase;
@property (nonatomic, strong) LangPhrase *mainLangPhrase;

@property (nonatomic, strong) CAGradientLayer *maskLayer;

@end

@implementation PhraseView

- (id)initWithFrame:(CGRect)frame bottomMargin:(CGFloat)bottomMargin
{
    self = [super initWithFrame:frame];
    if (self) {
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height - bottomMargin)];
        [self addSubview:scrollView];        
        self.scrollView = scrollView;        
        self.scrollView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
        self.scrollView.showsVerticalScrollIndicator = NO;
        self.scrollView.showsVerticalScrollIndicator = NO;
        self.scrollView.bounces = YES;
        self.scrollView.alwaysBounceVertical = YES;
        self.scrollView.delegate = self;
        self.scrollView.delaysContentTouches = NO;
        
        if (!self.maskLayer)
        {
            self.maskLayer = [CAGradientLayer layer];            
            
            self.maskLayer.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithWhite:1.0 alpha:1.0] CGColor],
                                     (id)[[UIColor colorWithWhite:1.0 alpha:0.0] CGColor], nil];
            self.maskLayer.locations = [NSArray arrayWithObjects:
                                   [NSNumber numberWithFloat:0.8],
                                   [NSNumber numberWithFloat:1.0], nil];
            
            self.maskLayer.bounds = CGRectMake(0, 0,
                                          self.scrollView.frame.size.width,
                                          self.scrollView.frame.size.height);
            self.maskLayer.anchorPoint = CGPointZero;
            
            self.scrollView.layer.mask = self.maskLayer;
            self.maskLayer.position = CGPointMake(0, scrollView.contentOffset.y);
        }
        
        if (IS_IPAD()) {
            UIImageView *backPanelView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phrase.back.panel.png"]];
            backPanelView.origin = CGPointMake((self.frame.size.width - backPanelView.frame.size.width) * 0.5, 10);
            [self.scrollView addSubview:backPanelView];
            self.backPanelView = backPanelView;
            self.backPanelView.userInteractionEnabled = YES;
            self.backPanelViewHeight = self.backPanelView.height;
        }
        
        SimpleBubbleView *topBubbleView = [[SimpleBubbleView alloc] initAsMain:YES];
        [topBubbleView.favoriteButton addTarget:self action:@selector(favoriteButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [topBubbleView.shareButton addTarget:self action:@selector(shareButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        if (!IS_IPAD()) {
            [self.scrollView addSubview:topBubbleView];
        } else {
            [self.backPanelView addSubview:topBubbleView];
        }
        self.topBubbleView = topBubbleView;
        
        SimpleBubbleView *bottomBubbleView = [[SimpleBubbleView alloc] initAsMain:NO];
        
        if (!IS_IPAD()) {
            [self.scrollView addSubview:bottomBubbleView];
        } else {
            [self.backPanelView addSubview:bottomBubbleView];
        }        
        self.bottomBubbleView = bottomBubbleView;                        
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        tap.delegate = self;
        [self.scrollView addGestureRecognizer:tap];
    }
    return self;
}

- (void)handleTap:(UITapGestureRecognizer *)tap {
    [self.topBubbleView deselectAll];
    [self.bottomBubbleView deselectAll];
}

- (void)favoriteButtonClicked {
    self.topBubbleView.favoriteButton.selected = !self.topBubbleView.favoriteButton.selected;    
    [MagicalRecord saveUsingCurrentThreadContextWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        if (self.topBubbleView.favoriteButton.selected) {
            self.phrase.favorite = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]];
        } else {
            self.phrase.favorite = @0;
        }
    }];
}

- (void)shareButtonClicked {
    if (self.delegate && [self.delegate respondsToSelector:@selector(phraseView:didClickedShareButton:withPhrase:withMainLangPhrase:)]) {
        [self.delegate phraseView:self didClickedShareButton:self.topBubbleView.shareButton withPhrase:self.phrase withMainLangPhrase:self.mainLangPhrase];
    }
}

- (void)populateWithMainLangPhrase:(LangPhrase *)mainLangPhrase secondLangPhrase:(LangPhrase *)secondLangPhrase isFavorite:(BOOL)isFavorite {    
    self.phrase = mainLangPhrase.phrase;
    self.mainLangPhrase = mainLangPhrase;
    self.topBubbleView.favoriteButton.selected = isFavorite;
    
    [self.topBubbleView populateWithLangPhrase:mainLangPhrase];
    self.topBubbleView.favoriteButton.selected = isFavorite;
    
    [self.bottomBubbleView populateWithLangPhrase:secondLangPhrase];
    
    if (IS_IPAD()) {
        CGFloat x = (self.scrollView.width - self.topBubbleView.width) * 0.5f;
        CGFloat xOffset = 70.0f;
        self.topBubbleView.origin = CGPointMake(x - xOffset, self.topBubbleView.origin.y);
        self.bottomBubbleView.origin = CGPointMake(x + xOffset, self.topBubbleView.bottom - 30.0f);
        
        //center bubble views
        
        CGFloat contentHeight = self.bottomBubbleView.bottom;
        if ((contentHeight + 60) > self.backPanelViewHeight) {
            self.backPanelView.height = contentHeight + 60;
        } else {
            self.backPanelView.height = self.backPanelViewHeight;
        }
        
        // center in back panel
        self.topBubbleView.origin = CGPointMake(self.topBubbleView.origin.x, (self.backPanelView.height - contentHeight) * 0.5f);
        self.bottomBubbleView.origin = CGPointMake(self.bottomBubbleView.origin.x, self.topBubbleView.bottom - 30.0f);
    } else {
        self.bottomBubbleView.origin = CGPointMake(self.topBubbleView.origin.x, self.topBubbleView.bottom + 10);
    }
    
    if (!IS_IPAD()) {
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.width, self.bottomBubbleView.bottom)];
    } else {
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.width, self.backPanelView.bottom)];
    }
    
    [self.scrollView setContentOffset:CGPointMake(0, -20) animated:NO];
    
    if (self.scrollView.contentOffset.y + self.scrollView.height >= self.scrollView.contentSize.height) {
        self.scrollView.layer.mask = nil;
    } else {
        self.scrollView.layer.mask = self.maskLayer;
    }
}

- (void)scrollToTop {
    [self.scrollView setContentOffset:CGPointMake(0, -20) animated:YES];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    self.maskLayer.position = CGPointMake(0, scrollView.contentOffset.y);
    [CATransaction commit];
    
    if (self.scrollView.contentOffset.y + self.scrollView.height >= self.scrollView.contentSize.height) {
        self.scrollView.layer.mask = nil;
    } else {
        self.scrollView.layer.mask = self.maskLayer;
    }
}

#pragma mark - UIGestureRecognizerDelegate methods
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (touch.view == self.topBubbleView.favoriteButton || touch.view == self.topBubbleView.shareButton) {
        return NO;
    } else {
        return YES;
    }
}

@end
