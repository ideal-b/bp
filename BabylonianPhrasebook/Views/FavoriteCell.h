//
//  FavoriteCell.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 03.02.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LangPhrase.h"

@interface FavoriteCell : UITableViewCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

- (void)populateWithLangPhrase:(LangPhrase *)phrase forIndexPath:(NSIndexPath *)indexPath;

+ (CGFloat)heightForLangPhrase:(LangPhrase *)phrase;

@end
