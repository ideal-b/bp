//
//  FavoriteCell.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 03.02.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "FavoriteCell.h"
#import "UIView+Sizes.h"
#import "UILabel+Size.h"
#import "FavoriteBackgroundCellView.h"

static const CGFloat sLabelWidth = 298.0;
static const CGFloat sTopMargin = 11.0;
static const CGFloat sBottomMargin = 11.0;
static const CGFloat sLeftMargin = 11.0;
static const CGFloat sMinHeight = 60.0;
static const CGFloat sOverlapOffset = 3.0;

@interface FavoriteCell()

@property (nonatomic, weak) FavoriteBackgroundCellView *backView;
@property (nonatomic, weak) UIImageView *backImageView;
@property (nonatomic, weak) UILabel *phraseLabel;

@end

@implementation FavoriteCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        self.clipsToBounds = NO;
        self.contentView.clipsToBounds = NO;
        
        FavoriteBackgroundCellView *view = [[FavoriteBackgroundCellView alloc] initWithFrame:CGRectZero];
        view.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:view];
        self.backView = view;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:view.frame];
        [self.contentView addSubview:imageView];
        self.backImageView = imageView;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(sLeftMargin, sTopMargin, sLabelWidth, 0)];
        label.backgroundColor = [UIColor clearColor];
        label.numberOfLines = 0;

        label.font = [UIFont systemFontOfSize:17];
        [self.contentView addSubview:label];
        self.phraseLabel = label;
    }
    return self;
}

- (void)populateWithLangPhrase:(LangPhrase *)phrase forIndexPath:(NSIndexPath *)indexPath {
    NSInteger drawType = indexPath.row % 3;
    
    CGFloat height = [[self class] heightForLangPhrase:phrase];
    
    UIImage *backgroundImage = [UIImage imageNamed:[NSString stringWithFormat:@"fav.cell.background_%d_%d.png", drawType, (NSInteger)(height + sOverlapOffset)]];
    if (backgroundImage) {
        self.backView.hidden = YES;
        self.backImageView.hidden = NO;
        self.backImageView.image = backgroundImage;
        self.backImageView.frame = CGRectMake(0, 0, 320, height + sOverlapOffset);
    } else {
        self.backView.hidden = NO;
        self.backImageView.hidden = YES;
        self.backView.frame = CGRectMake(0, 0, 320, height + sOverlapOffset);
        [self.backView setDrawType:drawType];
    }
    
    self.phraseLabel.text = phrase.text;
    [self.phraseLabel sizeToFitFixedWidth:sLabelWidth];
        
    self.phraseLabel.origin = CGPointMake(sLeftMargin, (height - self.phraseLabel.height) * 0.5);
}

+ (CGFloat)heightForLangPhrase:(LangPhrase *)phrase {
    CGFloat result = 0;
    result += sTopMargin + sBottomMargin;
    result += [phrase.text sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(sLabelWidth, 9999) lineBreakMode:UILineBreakModeWordWrap].height;    
    return MAX(sMinHeight, result);
}

@end
