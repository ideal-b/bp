//
//  CategoryCell.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 30.03.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Category.h"

@interface CategoryCell : UITableViewCell

- (void)populateWithCategory:(Category *)category forIndexPath:(NSIndexPath *)indexPath;

+ (CGFloat)heightForIndexPath:(NSIndexPath *)indexPath;

@end
