//
//  PhraseLastView.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 05.04.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"

@interface PhraseLastView : UIView <SwipeItemView>

@property (nonatomic, strong) NSString *reuseIdentifier;

@end
