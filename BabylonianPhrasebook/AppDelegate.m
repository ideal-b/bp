//
//  AppDelegate.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 15.01.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "AppDelegate.h"
#import "UIColor+RGB.h"
#import "ImageCacheGenerator.h"
#import <Crashlytics/Crashlytics.h>
#import "PBDataManager.h"
#import "PBTabbarViewController.h"
#import "Flurry.h"
#import "PlayHavenSDK.h"
#import "MBProgressHUD.h"
#import "SHK.h"
#import "BPSHKConfigurator.h"
#import "SHKConfiguration.h"
#import "iRate.h"
#import "SHKSharer.h"

#define kStoreName @"Phrasebook.db"

@interface AppDelegate()

@end

@implementation AppDelegate

+ (void)initialize {
    //configure iRate
    iRate *irate = [iRate sharedInstance];
    irate.daysUntilPrompt = 0;
    irate.usesUntilPrompt = 10;
    
#ifdef BP_LITE
    irate.eventsUntilPrompt = 19;
#else
    irate.eventsUntilPrompt = 57;
#endif
    
    irate.promptAgainForEachNewVersion = YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    
    [Crashlytics startWithAPIKey:@"f258c410e2657abb8695853e8d6f53e54919de4e"];
    
    [Flurry startSession:@"JFFWQPXWK7NRBQHBKBTZ"];
    
    DefaultSHKConfigurator *configurator = [[BPSHKConfigurator alloc] init];
    [SHKConfiguration sharedInstanceWithConfigurator:configurator];
    
    if (!IS_IPAD()) {
        [[UINavigationBar appearanceWhenContainedIn:[PBTabbarViewController class], nil] setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
        
        [[UINavigationBar appearanceWhenContainedIn:[PBTabbarViewController class], nil] setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:
          [UIColor colorWithRGBHexString:@"71b9c4"],
          UITextAttributeTextColor,
          [UIFont fontWithName:@"DR Agu" size:18],
          UITextAttributeFont,
          [UIColor clearColor],
          UITextAttributeTextShadowColor,
          [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 0.0f)],
          UITextAttributeTextShadowOffset,
          nil]];
        
        [[UINavigationBar appearanceWhenContainedIn:[PBTabbarViewController class], nil] setTitleVerticalPositionAdjustment:4.0 forBarMetrics:UIBarMetricsDefault];
    }
    
    [PBDataManager initializeData];
    
    // show splash
    [self.window makeKeyAndVisible];    
    
    UIImage *splashImage;
    if (IS_IPAD()) {
        splashImage = [UIImage imageNamed:@"Default-Landscape.png"];
    } else {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height > 480.0f) {
            splashImage = [UIImage imageNamed:@"Default-568h.png"];
        } else {
            splashImage = [UIImage imageNamed:@"Default.png"];
        }
    }
    
    UIImageView *splashView = [[UIImageView alloc] initWithImage:splashImage];
    if (IS_IPAD()) {
        if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeLeft) {
            splashView.transform = CGAffineTransformMakeRotation(-M_PI_2);
            splashView.frame = CGRectMake(20, 0, splashView.frame.size.width, splashView.frame.size.height);
        } else if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeRight) {
            splashView.transform = CGAffineTransformMakeRotation(M_PI_2);
            splashView.frame = CGRectMake(0, 0, splashView.frame.size.width, splashView.frame.size.height);
        }
        
    }
    [self.window addSubview:splashView];
    
    [self performSelector:@selector(hideSplashView:) withObject:splashView afterDelay:2.0f];
    
#ifdef BP_LITE
    [self showPlayHavenPlacement];
#endif
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sharerFinishedSending:) name:@"SHKSendDidFinish" object:nil];
    
    return YES;
}

- (void)sharerFinishedSending:(NSNotification *)notification {
    SHKSharer *sharer = notification.object;
    NSString *shareTitle = sharer.sharerTitle;
    if (!shareTitle) {
        shareTitle = @"N/A";
    }   
    [Flurry logEvent:@"Social" withParameters:@{ @"Share Phrase": shareTitle }];
}

- (void)hideSplashView:(UIView *)splashView {
    [UIView animateWithDuration:0.3f delay:.0f options:UIViewAnimationCurveEaseOut animations:^{
        splashView.alpha = 0;
    } completion:^(BOOL finished) {
        [splashView removeFromSuperview];
    }];
}

- (void)showPlayHavenPlacement {
    NSString *key = @"bp-date-last-show-playhaven";
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    BOOL shouldShow = NO;
    
    NSDate *lastShownDate = [defaults objectForKey:key];
    if (!lastShownDate) {
        shouldShow = YES;
    } else {
        NSDate *today = [NSDate date];
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                            fromDate:lastShownDate
                                                              toDate:today
                                                             options:0];
        
        if (components.day >= 1) {
            shouldShow = YES;
        }
    }
    
    if (shouldShow) {                    
        PHPublisherContentRequest *request = [PHPublisherContentRequest requestForApp:@"e47a438a3339400783ddddca490fd474" secret:@"24c46cd11be745ab9ee4733de9ea7af8" placement:@"game_launch" delegate:self];
        [request send];
        
        [MBProgressHUD showHUDAddedTo:self.window animated:YES];
        
        [defaults setObject:[NSDate date] forKey:key];
        [defaults synchronize];
        
        [Flurry logEvent:@"Social" withParameters:@{ @"Game launch": @"Opened PlayHaven" }];
    }
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [MagicalRecord cleanUp];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - PlayHaven delegate
- (void)request:(PHPublisherContentRequest *)request contentDidDisplay:(PHContent *)content {
    [MBProgressHUD hideHUDForView:self.window animated:YES];
}

- (void)request:(PHPublisherContentRequest *)request didFailWithError:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.window animated:YES];
}

- (void)requestDidFinishLoading:(PHAPIRequest *)request {
    [MBProgressHUD hideHUDForView:self.window animated:YES];
}

@end
