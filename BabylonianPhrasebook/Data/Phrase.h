//
//  Phrase.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 24.02.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Category, LangPhrase;

@interface Phrase : NSManagedObject

@property (nonatomic, retain) NSNumber * favorite;
@property (nonatomic, retain) NSNumber * sortOrder;
@property (nonatomic, retain) Category *category;
@property (nonatomic, retain) NSSet *langPhrases;
@end

@interface Phrase (CoreDataGeneratedAccessors)

- (void)addLangPhrasesObject:(LangPhrase *)value;
- (void)removeLangPhrasesObject:(LangPhrase *)value;
- (void)addLangPhrases:(NSSet *)values;
- (void)removeLangPhrases:(NSSet *)values;

@end
