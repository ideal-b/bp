//
//  PBDataManager.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 09.04.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PBDataManager : NSObject

+ (void)initializeData;

@end
