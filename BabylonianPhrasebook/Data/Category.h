//
//  Category.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 24.02.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Phrase;

@interface Category : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * name_de;
@property (nonatomic, retain) NSString * name_et;
@property (nonatomic, retain) NSString * name_en;
@property (nonatomic, retain) NSString * name_es;
@property (nonatomic, retain) NSString * name_fr;
@property (nonatomic, retain) NSString * name_ru;
@property (nonatomic, retain) NSNumber * sortOrder;
@property (nonatomic, retain) NSSet *phrases;

@property (nonatomic, readonly) NSString *name;

@end

@interface Category (CoreDataGeneratedAccessors)

- (void)addPhrasesObject:(Phrase *)value;
- (void)removePhrasesObject:(Phrase *)value;
- (void)addPhrases:(NSSet *)values;
- (void)removePhrases:(NSSet *)values;

@end
