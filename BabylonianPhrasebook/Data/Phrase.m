//
//  Phrase.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 24.02.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "Phrase.h"
#import "Category.h"
#import "LangPhrase.h"


@implementation Phrase

@dynamic favorite;
@dynamic sortOrder;
@dynamic category;
@dynamic langPhrases;

@end
