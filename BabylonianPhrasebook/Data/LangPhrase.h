//
//  LangPhrase.h
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 24.02.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Phrase;

@interface LangPhrase : NSManagedObject

@property (nonatomic, retain) NSString * lang;
@property (nonatomic, retain) NSNumber * sortOrder;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * trans_ru;
@property (nonatomic, retain) Phrase *phrase;

@end
