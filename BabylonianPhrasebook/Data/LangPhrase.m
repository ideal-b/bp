//
//  LangPhrase.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 24.02.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "LangPhrase.h"
#import "Phrase.h"


@implementation LangPhrase

@dynamic lang;
@dynamic sortOrder;
@dynamic text;
@dynamic trans_ru;
@dynamic phrase;

@end
