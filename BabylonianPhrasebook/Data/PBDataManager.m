//
//  PBDataManager.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 09.04.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "PBDataManager.h"
#import "Phrase.h"
#import "LangPhrase.h"

#define kStoreName @"Phrasebook.db"
#define kVersionKey @"phrasebook-data-version"

@implementation PBDataManager

+ (void)initializeData {
    [self importInitialData];
    [MagicalRecord setupCoreDataStackWithStoreNamed:kStoreName];
    
    [Phrase MR_setDefaultBatchSize:10];
    [LangPhrase MR_setDefaultBatchSize:10];
}

+ (void)importInitialData {
    [self applicationSupportDirectory];
    NSURL *storeURL = [NSPersistentStore MR_urlForStoreName:kStoreName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL fileExists = [fileManager fileExistsAtPath:[storeURL path]];
    
    BOOL shouldImportData = !fileExists;
    
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"GitVersion"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (fileExists) {
        NSString *savedAppVersion = [defaults stringForKey:kVersionKey];
        if (![savedAppVersion isEqualToString:appVersion]) {
            NSError *error;
            shouldImportData = YES;
            [fileManager removeItemAtPath:[storeURL path] error:&error];
            NSAssert(error == nil, @"Couldn't delete old data");
        }
    }
    
    if (shouldImportData) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Phrasebook" ofType:@"db"];
        NSError *error;
        [fileManager copyItemAtPath:path toPath:[storeURL path] error:&error];
        NSAssert(error == nil, @"Error initialization data is not nil");
        [defaults setObject:appVersion forKey:kVersionKey];
        [defaults synchronize];
    }
}

+ (NSString *)findOrCreateDirectory:(NSSearchPathDirectory)searchPathDirectory
                           inDomain:(NSSearchPathDomainMask)domainMask
                appendPathComponent:(NSString *)appendComponent
                              error:(NSError **)errorOut
{
    // Search for the path
    NSArray* paths = NSSearchPathForDirectoriesInDomains(
                                                         searchPathDirectory,
                                                         domainMask,
                                                         YES);
    if ([paths count] == 0)
    {
        // *** creation and return of error object omitted for space
        return nil;
    }
    
    // Normally only need the first path
    NSString *resolvedPath = [paths objectAtIndex:0];
    
    if (appendComponent)
    {
        resolvedPath = [resolvedPath
                        stringByAppendingPathComponent:appendComponent];
    }
    
    // Create the path if it doesn't exist
    NSError *error;
    BOOL success = [[NSFileManager defaultManager]
                    createDirectoryAtPath:resolvedPath
                    withIntermediateDirectories:YES
                    attributes:nil
                    error:&error];
    if (!success)
    {
        if (errorOut)
        {
            *errorOut = error;
        }
        return nil;
    }
    
    // If we've made it this far, we have a success
    if (errorOut)
    {
        *errorOut = nil;
    }
    return resolvedPath;
}

+ (NSString *)applicationSupportDirectory
{
    NSString *executableName =
    [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleExecutable"];
    NSError *error;
    NSString *result =
    [self
     findOrCreateDirectory:NSApplicationSupportDirectory
     inDomain:NSUserDomainMask
     appendPathComponent:executableName
     error:&error];
    if (error)
    {
        NSLog(@"Unable to find or create application support directory:\n%@", error);
    }
    return result;
}


@end
