//
//  Category.m
//  BabylonianPhrasebook
//
//  Created by Oleg Poyaganov on 24.02.13.
//  Copyright (c) 2013 opedge. All rights reserved.
//

#import "Category.h"
#import "Phrase.h"


@implementation Category

@dynamic id;
@dynamic name_de;
@dynamic name_et;
@dynamic name_en;
@dynamic name_es;
@dynamic name_fr;
@dynamic name_ru;
@dynamic sortOrder;
@dynamic phrases;

- (NSString *)name {
    return [self valueForKey:[NSString stringWithFormat:@"name_%@", BPCurrentLanguage()]];
}

@end
