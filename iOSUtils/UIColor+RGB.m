//
//  UIColor+RGB.m
//  iOSUtilsDemo
//
//  Created by Oleg Poyaganov on 15.01.13.
//  Copyright (c) 2013 Hyperboloid. All rights reserved.
//

#import "UIColor+RGB.h"

@implementation UIColor (RGB)

+ (UIColor *)colorWithRGBHexString:(NSString *)rgbHexString {
    return [self colorWithRGBAHexString:[rgbHexString stringByAppendingString:@"ff"]];
}

+ (UIColor *)colorWithRGBAHexString:(NSString *)rgbaHexString {    
    NSAssert([rgbaHexString length] == 8, @"RGBA hex string length not equal 8 characters");
    
    NSString *hex = [rgbaHexString uppercaseString];
    
    // Separate into r, g, b, a substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [hex substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [hex substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [hex substringWithRange:range];
    
    range.location = 6;
    NSString *aString = [hex substringWithRange:range];
    
    unsigned int r, g, b, a;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    [[NSScanner scannerWithString:aString] scanHexInt:&a];
    
    return [UIColor colorWithRed:((CGFloat) r / 255.0f)
                           green:((CGFloat) g / 255.0f)
                            blue:((CGFloat) b / 255.0f)
                           alpha:((CGFloat) a / 255.0f)];
    
}

+ (UIColor *)colorWithR:(NSUInteger)red G:(NSUInteger)green B:(NSUInteger)blue {
    return [self colorWithR:red G:green B:blue A:255];
}

+ (UIColor *)colorWithR:(NSUInteger)red G:(NSUInteger)green B:(NSUInteger)blue A:(NSUInteger)alpha {
    return [UIColor colorWithRed:((CGFloat) red / 255.0f)
                           green:((CGFloat) green / 255.0f)
                            blue:((CGFloat) blue / 255.0f)
                           alpha:((CGFloat) alpha / 255.0f)];
}

@end
