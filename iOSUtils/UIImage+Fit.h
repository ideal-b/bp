//
//  UIImage+Fit.h
//  iOSUtilsDemo
//
//  Created by Oleg Poyaganov on 15.01.13.
//  Copyright (c) 2013 Hyperboloid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Fit)

- (UIImage *)imageByScalingAndCroppingForSize:(CGSize)targetSize;

@end
