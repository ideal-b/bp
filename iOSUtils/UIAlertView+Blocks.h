//
//  UIAlertView+Blocks.h
//  iOSUtils
//
//  Created by Oleg Poyaganov on 1/14/13.
//  Copyright (c) 2013 Hyperboloid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Blocks)

typedef void (^DismissBlock)(int buttonIndex);
typedef void (^CancelBlock)();

+ (UIAlertView *)showAlertViewWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSArray *)otherButtons onDismiss:(DismissBlock)dismissed onCancel:(CancelBlock)cancelled;

@end
