//
//  UIColor+RGB.h
//  iOSUtilsDemo
//
//  Created by Oleg Poyaganov on 15.01.13.
//  Copyright (c) 2013 Hyperboloid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RGB)

+ (UIColor *)colorWithRGBHexString:(NSString *)rgbHexString;

+ (UIColor *)colorWithRGBAHexString:(NSString *)rgbaHexString;

+ (UIColor *)colorWithR:(NSUInteger)red G:(NSUInteger)green B:(NSUInteger)blue;

+ (UIColor *)colorWithR:(NSUInteger)red G:(NSUInteger)green B:(NSUInteger)blue A:(NSUInteger)alpha;

@end
