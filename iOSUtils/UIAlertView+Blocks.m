//
//  UIAlertView+Blocks.m
//  iOSUtils
//
//  Created by Oleg Poyaganov on 1/14/13.
//  Copyright (c) 2013 Hyperboloid. All rights reserved.
//

#import "UIAlertView+Blocks.h"

@implementation UIAlertView (Blocks)

static DismissBlock _dismissBlock;
static CancelBlock _cancelBlock;

+ (UIAlertView *)showAlertViewWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSArray *)otherButtons onDismiss:(DismissBlock)dismissed onCancel:(CancelBlock)cancelled {
    
    _cancelBlock = nil;
    _cancelBlock = [cancelled copy];
    
    _dismissBlock = nil;
    _dismissBlock = [dismissed copy];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:[self self] cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
    
    for (NSString *buttonTitle in otherButtons) {
        [alert addButtonWithTitle:buttonTitle];
    }
    
    [alert show];
    return alert;
}

+ (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [alertView cancelButtonIndex]) {
        if (_cancelBlock) {
            _cancelBlock();
        }
    } else {
        if (_dismissBlock) {
            _dismissBlock(buttonIndex - 1);
        }        
    }
}

@end
