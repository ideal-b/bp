//
//  UIView+Sizes.h
//  iOSUtils
//
//  Created by Oleg Poyaganov on 1/14/13.
//  Copyright (c) 2013 Hyperboloid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Sizes)

@property (nonatomic) CGFloat left;
@property (nonatomic) CGFloat top;
@property (nonatomic) CGFloat right;
@property (nonatomic) CGFloat bottom;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;

@property (nonatomic) CGPoint origin;
@property (nonatomic) CGSize size;

@end
