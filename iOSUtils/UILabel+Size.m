//
//  UILabel+Size.m
//  iOSUtilsDemo
//
//  Created by Oleg Poyaganov on 15.01.13.
//  Copyright (c) 2013 Hyperboloid. All rights reserved.
//

#import "UILabel+Size.h"

@implementation UILabel (Size)

- (CGSize)sizeToFitFixedWidth:(CGFloat)fixedWidth
{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, fixedWidth, 0);
    self.numberOfLines = 0;
    [self sizeToFit];
    return self.frame.size;
}

@end
